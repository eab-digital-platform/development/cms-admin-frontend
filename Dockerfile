FROM node:12.2.0-alpine
EXPOSE 3000

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY . /app

RUN wget http://192.168.225.247:48080/nm.tar.gz -P /app
RUN cd /app && tar zxf nm.tar.gz

# start app

#RUN npm start
CMD ["npm", "start"]
