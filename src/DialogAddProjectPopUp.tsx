import React, { FunctionComponent, useState, useContext } from "react";
import classnames from "classnames";
import { RouteContext } from "./contextComponent";
import { TextField, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { PresetDetail, DialogBasicProps } from "./type/interfaceCommon";

const DialogAddProjectPopUp: FunctionComponent<DialogBasicProps> = (
  props: DialogBasicProps
) => {
  const [presetPageInfo, setPresetPageInfo] = useState<PresetDetail>({
    code: "",
    description: ""
  });
  const { func } = useContext(RouteContext);
  // const { addedItemList = [] } = state;
  // const [filteredItemList, setFilteredItemList] = useState<TreeDataStructure[]>(
  //   []
  // );
  const { openErrorDialogWithMsg } = func;
  // );
  const {
    // addItemToTree,
    closeDialog,
    dialogTopLeftTitle,
    dialogOpen,
    onSubmit,
    namePlaceHolder,
    descPlaceHolder
  } = props;

  const useStyles = makeStyles({
    main: {
      width: "100%"
    },
    searchBar: {
      width: "600px"
    },
    dialogContent: {
      height: "400px",
      display: "grid",
      gridTemplateRows: "5fr 2fr",
      padding: "0 var(--Apadding-L)"
    },
    mainContainer: {
      padding: "var(--Apadding-L)",
      flex: "5 0 0"
    },
    title: {
      fontWeight: 700,
      fontSize: "var(--Afont-M)"
    },
    menuItem: {
      height: "42px",
      fontSize: "var(--Afont-S)",
      lineHeight: "42px",
      textAlign: "left",
      padding: "0 var(--Apadding-XXXM)"
    },
    selectedItem: {
      color: "var(--Aprimary-color)",
      borderLeft: "var(--Aborder-L) var(--Aprimary-color-light1) solid",
      paddingLeft: "var(--Apadding-M)"
    },
    normalBorder: {
      border: "var(--Aborder-S) var(--Aline-color) solid",
      maxWidth: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)"
    },
    selectedIcon: {
      border: "var(--Aborder-S) var(--Aprimary-color) solid",
      maxWidth: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)"
    },
    pageCode: {
      height: "46px",
      marginBottom: "var(--Apadding-XXXS)"
    },
    titleMain: {
      alignItems: "center",
      fontSize: "var(--Afont-XM)"
    },
    presetTitle: {
      fontSize: "var(--Afont-XS)",
      paddingBottom: "var(--Apadding-XS)"
    },
    buttonWrapper: {
      padding: "var(--Apadding-XS) var(--Apadding-L) var(--Apadding-L)"
    }
  });

  const useStylesTypo = makeStyles(
    {
      root: {
        display: "flex"
      }
    },
    { name: "MuiTypography" }
  );
  const useStylesText = makeStyles(
    {
      root: {
        height: "46px"
      }
    },
    { name: "MuiOutlinedInput" }
  );
  const classes = useStyles(props);
  const classesTypo = useStylesTypo(props);
  const classText = useStylesText(props);
  // const addItemHandler = (): void => {
  //   addItemToTree(keyOfAddingItem, selectedAddingItem);
  //   closeDialog();
  // };

  const onAddHandler = (): void => {
    const { code, description } = presetPageInfo;
    if (code) {
      const data = {
        description: {
          en: description
        },
        title: {
          en: code
        }
      };
      if (onSubmit) {
        onSubmit(data)
          .then(() => {
            closeDialog();
          })
          .catch(err => {
            openErrorDialogWithMsg(err);
          });
      }
    }
  };

  const changeCode = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    const newPreset = { ...presetPageInfo, code: e.target.value };
    setPresetPageInfo(newPreset);
  };

  const changeDescription = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    setPresetPageInfo({ ...presetPageInfo, description: e.target.value });
  };
  const { code, description } = presetPageInfo;
  return (
    <Dialog
      //   className={classes.dialogGrid}
      open={dialogOpen}
      onClose={closeDialog}
      maxWidth={"xs"}
    >
      <DialogTitle className={classesTypo.root} id="simple-dialog-title">
        <div className={classnames(classes.titleMain)}>
          <p>{dialogTopLeftTitle}</p>
        </div>
      </DialogTitle>
      <DialogContent className={classnames(classesTypo.root)} dividers>
        <div className={classes.main}>
          {/* <div className={classes.presetDetail}> */}
          <div>
            <p className={classes.presetTitle}>Preset Details</p>
            <TextField
              className={classnames(classes.pageCode, classText.root)}
              autoFocus
              fullWidth
              error={!code}
              margin="none"
              id="outlined-basic"
              variant="outlined"
              value={code}
              onChange={changeCode}
              placeholder={namePlaceHolder || "Project Name"} // TODO: need to modify
            />
            <TextField
              className={classText.root}
              fullWidth
              margin="none"
              id="outlined-basic"
              variant="outlined"
              value={description}
              onChange={changeDescription}
              placeholder={descPlaceHolder || "Description"} // TODO: need to modify
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions className={classes.buttonWrapper}>
        <Button onClick={closeDialog}>Cancel</Button>
        <Button
          disabled={!code}
          onClick={onAddHandler}
          variant="contained"
          color="primary"
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogAddProjectPopUp;
