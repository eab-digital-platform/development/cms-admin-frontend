import React, {
  FunctionComponent,
  useState,
  useContext,
  useEffect
} from "react";
import classnames from "classnames";
import { RouteContext } from "./contextComponent";
import {
  TextField,
  Radio,
  Button,
  RadioGroup,
  FormControlLabel
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { addPage, getPageJson, getListOfPage } from "./api";
import { parsePageList, parseNewTreeStructure } from "./parser";
import { routeType } from "./type/enum";
import {
  DialogBasicProps,
  TreeDataStructure,
  PresetDetail
} from "./type/interfaceCommon";

const DialogAddPagePopUp: FunctionComponent<DialogBasicProps> = (
  props: DialogBasicProps
) => {
  const [searchText, setSearchText] = useState<string>("");
  const [selectedAddingItem, setSelectedAddingItem] = useState<string>("");
  const [presetPageInfo, setPresetPageInfo] = useState<PresetDetail>({
    code: "",
    description: ""
  });
  const { code, description } = presetPageInfo;
  const { state, func } = useContext(RouteContext);
  const { pageTemplateList = [], currentProjectId } = state;
  const [filteredItemList, setFilteredItemList] = useState<TreeDataStructure[]>(
    []
  );
  const {
    setCurrentPageId,
    setTreeDataState,
    setRouteState,
    setPageList,
    openErrorDialogWithMsg
  } = func;
  // const [selectedCategory, setSelectedCategory] = useState<itemCategory | "">(
  //   itemCategory.Inputs
  // );
  const {
    // addItemToTree,
    closeDialog,
    dialogTopLeftTitle,
    dialogOpen
    // keyOfAddingItem
  } = props;
  let error = true;
  if (code && selectedAddingItem) {
    error = false;
  } else {
    error = true;
  }

  useEffect(() => {
    if (dialogOpen === false) {
      setSelectedAddingItem("");
      console.warn("reset the value");
    }
  }, [dialogOpen]);

  useEffect(() => {
    setFilteredItemList(pageTemplateList);
  }, []);

  const useStyles = makeStyles({
    rightSide: {
      // width: "150px"
      flex: "2 0 0",
      padding:
        "var(--Apadding-M) var(--Apadding-L) var(--Apadding-M) var(--Apadding-XXS)"
    },
    searchBar: {
      width: "600px"
    },
    dialogContent: {
      height: "400px",
      display: "grid",
      gridTemplateRows: "5fr 2fr",
      padding: "0"
    },
    mainContainer: {
      padding: "var(--Apadding-L)",
      flex: "5 0 0",
      "& .MuiFormControlLabel-labelPlacementBottom": {
        overflow: "hidden"
      }
    },
    title: {
      fontWeight: 700,
      fontSize: "var(--Afont-M)"
    },
    menuItem: {
      height: "42px",
      fontSize: "var(--Afont-S)",
      lineHeight: "42px",
      textAlign: "left",
      padding: "0 var(--Apadding-XXXM)"
    },
    selectedItem: {
      color: "var(--Aprimary-color)",
      borderLeft: "var(--Aborder-L) var(--Aprimary-color-light1) solid",
      paddingLeft: "var(--Apadding-M)"
    },
    normalBorder: {
      border: "var(--Aborder-S) var(--Aline-color) solid",
      maxWidth: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)",
      overflow: "hidden"
    },
    selectedIcon: {
      border: "var(--Aborder-S) var(--Aprimary-color) solid",
      maxWidth: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)",
      overflow: "hidden"
    },
    pageCode: {
      height: "46px",
      marginBottom: "var(--Apadding-XXXS)"
    },
    titleMain: {
      display: "grid",
      gridTemplateColumns: "1fr 4fr 2fr",
      alignItems: "center",
      padding: "0 var(--Apadding-XXS)",
      fontSize: "var(--Afont-XM)"
    },
    topLeftTitle: {
      fontWeight: "bold",
      fontSize: "var(--Afont-XM)"
    },
    presetTitle: {
      fontSize: "var(--Afont-XS)",
      paddingBottom: "var(--Apadding-XS)"
    }
  });

  const useStylesTypo = makeStyles(
    {
      root: {
        display: "flex"
      }
    },
    { name: "MuiTypography" }
  );
  const useStylesRadio = makeStyles({
    colorPrimary: {
      "&.Mui-checked:hover": {
        backgroundColor: "transparent"
      },
      "&:hover": {
        backgroundColor: "transparent"
      }
    }
  });
  const useStylesText = makeStyles(
    {
      root: {
        height: "46px"
      }
    },
    { name: "MuiOutlinedInput" }
  );
  const classes = useStyles(props);
  const classesTypo = useStylesTypo(props);
  const classesRadio = useStylesRadio(props);
  const classText = useStylesText(props);

  const filterList = (searchText: string): void => {
    if (searchText && searchText.length > 0) {
      const updatedList = pageTemplateList.filter((item: TreeDataStructure) => {
        return (
          item.displayName.en.toLowerCase().search(searchText.toLowerCase()) !==
          -1
        );
      });
      setFilteredItemList(updatedList);
    } else {
      setFilteredItemList(pageTemplateList);
    }
  };

  const closeDialogClearValue = (): void => {
    closeDialog();
    setPresetPageInfo({
      code: "",
      description: ""
    });
  };

  const addPageHandler = (): void => {
    console.warn(
      "Adding your fucking project now!!!!! Please find your id!!!!: ",
      currentProjectId
    );
    const { AddHome } = routeType;
    const selectedJson = pageTemplateList.filter(temp => {
      return selectedAddingItem === temp._id;
    });
    const data = {
      ...selectedJson[0],
      description: {
        en: description
      },
      code: code,
      level: "HONG KONG"
    }; //TODO: add REAL LEVEL
    delete data._id;
    addPage(data, currentProjectId)
      .then(res => {
        setCurrentPageId(res);
        return getPageJson(res);
      })
      .catch(err => {
        openErrorDialogWithMsg(err.response.data.message);
      })
      .then(treeData => {
        if (treeData) {
          const parsedTree = parseNewTreeStructure(treeData);
          setTreeDataState([parsedTree]);
          setRouteState(AddHome);
          getListOfPage(currentProjectId).then(data => {
            const pageList = parsePageList(data); // TODO: change to parse pj list
            setPageList(pageList);
          });
          closeDialog();
        } else {
          console.error("Error, something wrong");
        }
      });
  };

  const radioOnChnage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setSelectedAddingItem(event.target.value);
  };

  const changeCode = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    const newPreset = { ...presetPageInfo, code: e.target.value };
    setPresetPageInfo(newPreset);
  };

  const changeDescription = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ): void => {
    setPresetPageInfo({ ...presetPageInfo, description: e.target.value });
  };
  return (
    <Dialog open={dialogOpen} onClose={closeDialog} fullWidth maxWidth={"lg"}>
      <DialogTitle className={classesTypo.root} id="simple-dialog-title">
        <div className={classnames(classes.mainContainer, classes.titleMain)}>
          <p className={classes.topLeftTitle}>{dialogTopLeftTitle}</p>
          <TextField
            className={classes.searchBar}
            fullWidth
            margin="none"
            id="outlined-basic"
            variant="outlined"
            value={searchText}
            onChange={(e): void => {
              setSearchText(e.target.value);
              filterList(e.target.value);
            }}
            placeholder={"Search page..."} // TODO: need to modify
          />
          <div></div>
        </div>
      </DialogTitle>
      <DialogContent
        className={classnames(classesTypo.root, classes.dialogContent)}
        dividers
      >
        <div className={classes.mainContainer}>
          <RadioGroup
            aria-label="position"
            name="position"
            value={selectedAddingItem}
            onChange={radioOnChnage}
            row
          >
            {filteredItemList.map(
              (item, index): JSX.Element => {
                const { displayName, imageUrl, _id } = item;
                const { en } = displayName;
                return (
                  <FormControlLabel
                    // checked={type === selectedAddingItem}
                    className={
                      selectedAddingItem === _id
                        ? classes.selectedIcon
                        : classes.normalBorder
                    }
                    key={index}
                    value={_id}
                    labelPlacement={"bottom"}
                    control={
                      <Radio
                        className={classesRadio.colorPrimary}
                        checkedIcon={<img src={imageUrl} alt={""} />}
                        icon={<img src={imageUrl} alt={""} />}
                        color="primary"
                        disableRipple
                      />
                    }
                    label={en}
                    // name="addingItem"
                  />
                );
              }
            )}
          </RadioGroup>
        </div>
        <div id={"rightMenuContainer"} className={classes.rightSide}>
          {/* <div className={classes.presetDetail}> */}
          <div>
            <p className={classes.presetTitle}>Preset Details</p>
            <TextField
              autoFocus
              className={classnames(classes.pageCode, classText.root)}
              fullWidth
              error={!code}
              margin="none"
              id="outlined-basic"
              variant="outlined"
              value={code}
              onChange={changeCode}
              placeholder={"Page Name"} // TODO: need to modify
            />
            <TextField
              className={classText.root}
              fullWidth
              margin="none"
              id="outlined-basic"
              variant="outlined"
              value={description}
              onChange={changeDescription}
              placeholder={"Description"} // TODO: need to modify
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={closeDialogClearValue}
          variant="contained"
          color="primary"
        >
          Cancel
        </Button>
        {console.warn("selectedAddingItem", selectedAddingItem, error)}
        <Button
          disabled={error}
          onClick={addPageHandler}
          variant="contained"
          color="primary"
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogAddPagePopUp;
