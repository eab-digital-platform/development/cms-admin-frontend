import React, {
  FunctionComponent,
  useContext,
  useState,
  useEffect,
  Fragment
} from "react";
import { RouteContext } from "./contextComponent";
import classnames from "classnames";
import { routeType } from "./type/enum";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button, InputAdornment, Tooltip } from "@material-ui/core";
import {
  PageInterface,
  Feb27ProjectInterface
  // ComponentSetStructure
} from "./type/interfaceCommon";
import {
  getPageJson,
  getListOfPage,
  deleteProject,
  getListOfProject,
  deletePage,
  addProject,
  clonePage,
  cloneProject,
  addComponentSet
  // addPjComponentSet
} from "./api";
import {
  parsePageList,
  parseNewTreeStructureList,
  parseProjectList,
  parseNewTreeStructure
} from "./parser";
import DialogAddPagePopUp from "./DialogAddPagePopUp";
import DialogAddProjectPopUp from "./DialogAddProjectPopUp";
import { ReactComponent as Copy } from "./assets/Copy.svg";
import { ReactComponent as Delete } from "./assets/Delete.svg";
import { ReactComponent as Edit } from "./assets/Edit.svg";
import Move from "./assets/Move.png";
import ConfirmDialog from "./common/ConfirmDialog";
import CopyDialog from "./common/CopyDialog";
import SettingUI from "./Setting";
import Search from "./assets/Search.png";
import { findTitleById } from "./common/commonFunction";
import DialogEditPagePopUp from "./DialogEditPagePopUp";
import config from "./config/config.json";

import styles from "./MainSection.module.css";
import DialogAddComptSetDialog from "./DialogAddComptSetPopUp";
import DialogEditPjComptSetPopUp from "./DialogEditPjComptSetPopUp";
// import PageSelector from "./components/PageSelector/PageSelector";

interface MainSectionProps {
  route?: string;
}

const MainSection: FunctionComponent<MainSectionProps> = (
  props: MainSectionProps
) => {
  const useStyles = makeStyles({
    sortingHeader: {
      // display: "grid",
      // gridTemplateColumns: "1fr 2fr 1fr 1fr",
      background: "var(--Aprimary-color-light1)",
      color: "var(--Awhite-font-color)",
      cursor: "default"
      // fontSize: "var(--Afont-general)",
      // padding: "var(--Apadding-S) 0 var(--Apadding-S) var(--Apadding-XL)",
      // gridTemplateColumns: "1fr 2fr 1fr 1fr 1fr 1fr"
    },
    rowItem: {
      fontSize: "var(--Afont-general)",
      padding: "var(--Apadding-XXS) 0 var(--Apadding-XXS) var(--Apadding-XL)",
      gridTemplateColumns: "2fr 4fr 2fr 2fr 2fr 2fr 1fr",
      display: "grid",
      alignItems: "center",
      height: "calc(100vh * (50/840))",
      "&:not($sortingHeader):hover": {
        backgroundColor: "var(--Aprimary-color-light3)",
        cursor: "pointer"
      }
    },
    rowItemComptSet: {
      fontSize: "var(--Afont-general)",
      padding: "var(--Apadding-XXS) 0 var(--Apadding-XXS) var(--Apadding-XL)",
      gridTemplateColumns: "2fr 5fr 2fr 2fr 1fr",
      display: "grid",
      alignItems: "center",
      height: "calc(100vh * (50/840))",
      "&:not($sortingHeader):hover": {
        backgroundColor: "var(--Aprimary-color-light3)",
        cursor: "pointer"
      }
    },
    rowItemProjectRoot: {
      fontSize: "var(--Afont-general)",
      padding: "var(--Apadding-XXS) 0 var(--Apadding-XXS) var(--Apadding-XL)",
      gridTemplateColumns: "2fr 5fr 2fr 2fr 1fr",
      display: "grid",
      cursor: "pointer",
      alignItems: "center",
      // height: "calc(100vh * (50/840))",
      height: "40px",
      "&:not($sortingHeader):hover": {
        backgroundColor: "var(--Aprimary-color-light3)",
        cursor: "pointer"
      }
    },
    rowItemComptSetRoot: {
      fontSize: "var(--Afont-general)",
      padding: "var(--Apadding-XXS) 0 var(--Apadding-XXS) var(--Apadding-XL)",
      gridTemplateColumns: "2fr 5fr 2fr 2fr 1fr",
      display: "grid",
      cursor: "pointer",
      alignItems: "center",
      height: "calc(100vh * (50/840))",
      "&:not($sortingHeader):hover": {
        backgroundColor: "var(--Aprimary-color-light3)",
        cursor: "pointer"
      }
    },
    selectedItem: {
      color: "var(--Aprimary-color)",
      borderLeft: "var(--Aborder-L) var(--Aprimary-color) solid",
      paddingLeft: "var(--Apadding-XXM)"
    },
    addContentContainer: {
      textAlign: "center",
      overflow: "hidden",
      height: "99%",
      width: "98%",
      position: "relative",
      margin: "0 auto"
    },
    addContentTitle: {
      fontSize: "var(--Afont-title)"
    },
    searchBar: {
      padding:
        "var(--Apadding-XXM) var(--Apadding-36) var(--Apadding-XXM) var(--Apadding-XL)",
      display: "grid",
      alignItems: "center",
      gridTemplateColumns: "1fr 77px",
      gridColumnGap: "var(--Apadding-XXXL)",
      "& .MuiOutlinedInput-root": {
        height: "36px"
      },
      "& .MuiOutlinedInput-input": {
        padding: "0"
      }
    },
    icon: {
      height: "24px",
      width: "24px"
    },
    functionalIconWrapper: {
      height: "24px",
      // overflow: "hidden"
      width: "100px"
    },
    moveIcon: {
      height: "24px",
      width: "24px"
    },
    mainContainer: {
      overflowX: "hidden",
      overflowY: "auto"
    },
    overflowText2Lines: {
      whiteSpace: "nowrap",
      textOverflow: "ellipsis",
      overflow: "hidden",
      padding: "0 var(--Apadding-XXXXS)"
      // display: "-webkit-box",
      // lineClamp: 2,
      // boxOrient: "vertical",
      // overflowWrap: "break-word"
    }
  });
  const classes = useStyles(props);
  const { state, func } = useContext(RouteContext);
  const {
    treeDataState,
    routeState,
    pageList,
    projectList,
    currentProjectId,
    errorObj,
    // currentCompId,
    addedItemList,
    componentSetList,
    pjComptSetList
  } = state;
  const { Index, AddProject, AddComponentSet, AddHome } = routeType;
  const {
    setRouteState,
    setTreeDataState,
    setCurrentPageId,
    setCurrentProjectId,
    setPageList,
    setProjectList,
    setCurrentCompId,
    openErrorDialogWithMsg,
    closeErrorDialog
  } = func;
  const { error, errorMsg } = errorObj;
  const [searchText, setSearchText] = useState<string>("");
  const [hoverId, setHoverId] = useState<string>("");
  const [targetId, setTarget] = useState<string>("");
  const [filteredList, setFilteredList] = useState<any[]>([]);
  const [comptSetFilteredList, setComptSetFilteredList] = useState<any[]>([]);
  const [openAddPageDialog, setOpenAddPageDialog] = useState<boolean>(false);
  const [openAddProjectDialog, setOpenAddProjectDialog] = useState<boolean>(
    false
  );
  const [openEditPageDialog, setOpenEditPageDialog] = useState<boolean>(false);
  // const [openCopyDialog, setOpenCopyDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openMoveDialog, setOpenMoveDialog] = useState<boolean>(false);
  const [selectedPage, setSelectedPage] = useState<any>({});

  useEffect(() => {
    console.warn("[MainSection.tsx] useEffect:: hell");
    if (routeState === Index) {
      setFilteredList(pageList);
    } else if (routeState === AddProject) {
      setFilteredList(projectList);
    } else if (routeState === AddHome) {
      const childFrameObj: any = document.getElementById("webuiIframe");
      if (childFrameObj) {
        setTimeout(() => {
          if (childFrameObj.contentWindow !== null) {
            console.log("Post message to iframe", ...treeDataState);
            childFrameObj.contentWindow.postMessage(...treeDataState, "*");
          }
        }, 1000);
      }
    } else if (routeState === AddComponentSet) {
      setComptSetFilteredList(pjComptSetList);
    }
  }, [
    pageList,
    projectList,
    routeState,
    componentSetList,
    pjComptSetList,
    addedItemList,
    treeDataState
  ]);

  useEffect(() => {
    console.warn("[MainSection.tsx] useEffect:: routeState");
    setSearchText("");
    setHoverId("");
  }, [routeState]);

  useEffect(() => {
    console.warn("[MainSection.tsx] useEffect:: dialogChange");
    if (hoverId && (openDeleteDialog || openMoveDialog)) {
      setTarget(hoverId);
    }
  }, [openDeleteDialog, openMoveDialog, hoverId]);

  const escapeChar = (string): string => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
  };

  const filterList = (text: string): void => {
    let updatedList;
    const searchText = escapeChar(text);
    if (searchText && searchText.length > 0) {
      switch (routeState) {
        case Index:
          updatedList = pageList.filter((item: PageInterface) => {
            return (
              item.type.toLowerCase().search(searchText.toLowerCase()) !== -1 ||
              item.code.toLowerCase().search(searchText.toLowerCase()) !== -1 ||
              item.description.en
                .toLowerCase()
                .search(searchText.toLowerCase()) !== -1
            );
          });
          break;
        case AddProject:
          updatedList = projectList.filter((item: Feb27ProjectInterface) => {
            return (
              item.title.en.toLowerCase().search(searchText.toLowerCase()) !==
                -1 ||
              item.description.en
                .toLowerCase()
                .search(searchText.toLowerCase()) !== -1
            );
          });
          break;
        case AddComponentSet:
          // updatedList = componentSetList.filter((item: any) => {
          updatedList = pjComptSetList.filter((item: any) => {
            // return false;
            return (
              item.title.toLowerCase().search(searchText.toLowerCase()) !==
                -1 ||
              item.description
                .toLowerCase()
                .search(searchText.toLowerCase()) !== -1
            );
          });
          break;
      }
      if (routeState === AddComponentSet) {
        setComptSetFilteredList(updatedList);
      } else {
        setFilteredList(updatedList);
      }
    } else if (routeState === Index) {
      setFilteredList(pageList);
      // setFilteredList(routeState === Index ? pageList : projectList);
    } else if (routeState === AddProject) {
      setFilteredList(projectList);
    } else if (routeState === AddComponentSet) {
      // setFilteredList(componentSetList);
      // setFilteredList(pjComptSetList);
      setComptSetFilteredList(pjComptSetList);
    }
  };

  const onClickPageRow = async (uId: string): Promise<any> => {
    const { AddHome } = routeType;
    const treeData = await getPageJson(uId);
    if (treeData) {
      const parsedTree = parseNewTreeStructure(treeData);
      console.warn("treeData Get back", parsedTree);
      setTreeDataState([parsedTree]);
      setRouteState(AddHome);
      setCurrentPageId(uId);
    } else {
      console.error("Error, something wrong");
    }
  };

  const onDelete = (): void => {
    if (routeState === AddProject) {
      deleteProject(targetId)
        .then(result => {
          if (result instanceof Error) {
            alert("Project doesn't exists!");
          }
          return getListOfProject();
        })
        .catch(err => {
          openErrorDialogWithMsg(err);
        })
        .then(data => {
          const projectList = parseProjectList(data); // TODO: change to parse pj list
          setProjectList(projectList);
          setOpenDeleteDialog(false);
          setTarget("");
        });
    } else {
      // === index
      deletePage(targetId)
        .then(result => {
          if (result instanceof Error) {
            alert("Page doesn't exists!");
          }
          return getListOfPage(currentProjectId);
        })
        .catch(err => {
          console.error(err);
        })
        .then(data => {
          const pageList = parsePageList(data); // TODO: change to parse pj list
          setPageList(pageList);
          setOpenDeleteDialog(false);
          setTarget("");
        });
    }
  };

  const onClickProjectRow = async (projectId: string): Promise<any> => {
    const pageList = await getListOfPage(projectId); //TODO: useProjectId to fetch
    if (pageList) {
      const parsedPageList = parsePageList(pageList);
      setCurrentProjectId(projectId); //TODO need to change it to dynamic
      setPageList(parsedPageList);
      setRouteState(Index);
    } else {
      console.error("Error, something wrong");
    }
  };

  const addProjectHandler = (data: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      addProject(data)
        .then(res => {
          console.warn("Success!!!!", res); //projectId
          setCurrentProjectId(res);
          return getListOfProject();
        })
        .catch(err => {
          console.error(err);
          reject(err.response.data.message);
        })
        .then(data => {
          const projectList = parseProjectList(data); // TODO: change to parse pj list
          setProjectList(projectList);
          const PageList = parsePageList([]);
          setPageList(PageList);
          setRouteState(Index);
          resolve();
        });
      // return false if error
    });
  };

  const addCompSetHandler = (data: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      addComponentSet(data)
        .then(res => {
          console.warn("Success!!!!", res); //TODO: change it to component
          // setPageList(res);
          setCurrentCompId(res);
          return getListOfPage(); //TODO: need to return false if error
        })
        .catch(err => {
          reject(err.response.data.message);
        })
        .then(data => {
          if (data) {
            const PageList = parseNewTreeStructureList(data);
            setPageList(PageList);
            setRouteState(Index);
            resolve();
          } else {
            reject("Something wrong");
          }
        });
      // return false if error
    });
  };

  const [isOpenAddPjComptSetDialog, setIsOpenAddPjComptSetDialog] = useState<
    boolean
  >(false);
  const [comptSetName, setComptSetName] = useState<string>("");
  const [comptSetDesc, setSomptSetDesc] = useState<string>("");

  const [isOpenEditPjComptSetDialog, setIsOpenEditPjComptSetDialog] = useState<
    boolean
  >(false);
  const [selectedPjComptSet, setSelectedPjComptSet] = useState<any>({});

  const returnMainContent = (): JSX.Element | undefined | null => {
    const { Index, AddHome, AddProject, Setting } = routeType;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let projectCode = "";
    if (projectList && projectList.length > 0 && currentProjectId) {
      projectCode = findTitleById(projectList, currentProjectId) || "";
    }

    switch (routeState) {
      case AddProject:
        // TODO: for temp display
        // return <SettingUI />;
        // return (
        //   <PageSelector
        //     title="Page Dropdown"
        //     projectId={"5e6eec3f3803470031dff80d"}
        //     initialSelectedPage={null}
        //   />
        // );
        return (
          <Fragment>
            <div className={classes.searchBar}>
              <TextField
                margin="none"
                id="searchTextField"
                variant="outlined"
                value={searchText}
                // className={classText.root}
                onChange={(e): void => {
                  setSearchText(e.target.value);
                  filterList(e.target.value);
                }}
                placeholder={"Search Projects..."} // TODO: need to modify
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <img
                        className={classes.icon}
                        src={Search}
                        alt={"Search"}
                      />
                    </InputAdornment>
                  )
                }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={(): void => {
                  // func.setRouteState(routeType.AddHome);
                  setOpenAddProjectDialog(true);
                  // TODO: Change it to open add project dialog
                }}
                // className={classes.addIcon}
              >
                Add
              </Button>
            </div>
            <div
              className={classnames(
                classes.sortingHeader,
                classes.rowItemProjectRoot
              )}
            >
              <div className={styles.ProjectName}>Project Name</div>
              <div className={styles.ProjectDescription}>Description</div>
              <div className={styles.ProjectLastEditDate}>Last Edit Date</div>
              <div className={styles.ProjectLastEditBy}>Last Edit By</div>
              <div className={classes.functionalIconWrapper}></div>
            </div>
            {filteredList.map(project => {
              const {
                uId,
                // type,
                // displayName,
                // level,
                description,
                system
              } = project;
              const projectId = uId;
              const title = project.title || { en: "" };
              return (
                <div
                  className={classes.rowItemProjectRoot}
                  onClick={(): void => {
                    onClickProjectRow(projectId);
                  }}
                  key={projectId}
                  onMouseEnter={(): void => {
                    setHoverId(projectId);
                  }}
                  onMouseLeave={(): void => {
                    setHoverId("");
                  }}
                >
                  <div className={classes.overflowText2Lines}>{title.en}</div>
                  <div className={classes.overflowText2Lines}>
                    {description.en}
                  </div>
                  <div className={styles.ProjectLastEditDate}>
                    {system.updatedDate}
                  </div>
                  <div className={styles.ProjectLastEditBy}>
                    {system.updatedBy}
                  </div>
                  <div className={classes.functionalIconWrapper}>
                    {hoverId === projectId && (
                      <Fragment>
                        {/* projectId is the id of this row */}
                        <Tooltip title={"Clone"}>
                          <Copy
                            onClick={(event): void => {
                              event.stopPropagation();
                              cloneProject(uId)
                                .then(() => {
                                  return getListOfProject();
                                })
                                .then(data => {
                                  const projectList = parseProjectList(data);
                                  setProjectList(projectList);
                                })
                                .catch(err => {
                                  openErrorDialogWithMsg(
                                    err.response.data.message
                                  );
                                });
                            }}
                          />
                        </Tooltip>
                        <Tooltip title={"Delete"}>
                          <Delete
                            onClick={(event): void => {
                              event.stopPropagation();
                              setOpenDeleteDialog(true);
                            }}
                          />
                        </Tooltip>
                      </Fragment>
                    )}
                  </div>
                </div>
              );
            })}
          </Fragment>
        );
      case Index:
        return (
          <Fragment>
            <div className={classes.searchBar}>
              <TextField
                margin="none"
                id="outlined-basic"
                variant="outlined"
                value={searchText}
                // className={classText.root}
                onChange={(e): void => {
                  setSearchText(e.target.value);
                  filterList(e.target.value);
                }}
                placeholder={"Search Page..."}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <img
                        className={classes.icon}
                        src={Search}
                        alt={"Search"}
                      />
                    </InputAdornment>
                  )
                }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={(): void => {
                  // func.setRouteState(routeType.AddHome);
                  setOpenAddPageDialog(true);
                }}
                // className={classes.addIcon}
              >
                Add
              </Button>
            </div>

            <div id="list">
              <div
                className={classnames(classes.sortingHeader, classes.rowItem)}
              >
                <div className={styles.PagePageName}>Page Name</div>
                <div className={styles.PageDescription}>Description</div>
                <div className={styles.PageStyleType}>Style (Type)</div>
                <div className={styles.PageLevel}>Level</div>
                <div className={styles.PageLastEditDate}>Last Edit Date</div>
                <div className={styles.PageLastEditBy}>Last Edit By</div>
                <div className={classes.functionalIconWrapper}></div>
              </div>

              {filteredList.map((page, index) => {
                const {
                  code,
                  uId,
                  type,
                  // displayName,
                  level,
                  description,
                  system
                } = page;
                return (
                  <div
                    className={classes.rowItem}
                    onClick={(): void => {
                      onClickPageRow(uId);
                    }}
                    key={index}
                    onMouseEnter={(): void => {
                      setHoverId(uId);
                    }}
                    onMouseLeave={(): void => {
                      setHoverId("");
                    }}
                  >
                    <div className={classes.overflowText2Lines}>{code}</div>
                    <div className={classes.overflowText2Lines}>
                      {description.en}
                    </div>
                    <div className={styles.PageStyleType}>{type}</div>
                    <div className={styles.PageLevel}>{level}</div>
                    <div className={styles.PageLastEditDate}>
                      {system.updatedDate}
                    </div>
                    <div className={styles.PageLastEditBy}>
                      {system.updatedBy}
                    </div>
                    <div className={classes.functionalIconWrapper}>
                      {hoverId === uId && (
                        <Fragment>
                          <Tooltip title={"Clone"}>
                            <Copy
                              onClick={(event): void => {
                                event.stopPropagation();
                                clonePage(uId)
                                  .then(() => {
                                    return getListOfPage(currentProjectId);
                                  })
                                  .then(data => {
                                    const pageList = parsePageList(data);
                                    setPageList(pageList);
                                  })
                                  .catch(err => {
                                    openErrorDialogWithMsg(
                                      err.response.data.message
                                    );
                                  });
                              }}
                            />
                          </Tooltip>
                          <Tooltip title={"Edit"}>
                            <Edit
                              onClick={(event): void => {
                                event.stopPropagation();
                                setSelectedPage(page);
                                setOpenEditPageDialog(true);
                              }}
                            />
                          </Tooltip>
                          <Tooltip title={"Move"}>
                            <img
                              className={classes.moveIcon}
                              src={Move}
                              alt="move"
                              onClick={(event): void => {
                                event.stopPropagation();
                                setOpenMoveDialog(true);
                              }}
                            />
                          </Tooltip>
                          <Tooltip title={"Delete"}>
                            <Delete
                              onClick={(event): void => {
                                event.stopPropagation();
                                setOpenDeleteDialog(true);
                              }}
                            />
                          </Tooltip>
                        </Fragment>
                      )}
                    </div>
                  </div>
                );
              })}
            </div>
          </Fragment>
        );
      case AddComponentSet:
        return (
          <Fragment>
            <div className={classes.searchBar}>
              <TextField
                margin="none"
                id="outlined-basic"
                variant="outlined"
                value={searchText}
                // className={classText.root}
                onChange={(e): void => {
                  setSearchText(e.target.value);
                  filterList(e.target.value);
                }}
                placeholder={"Search Component Set..."} // TODO: need to modify
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <img
                        className={classes.icon}
                        src={Search}
                        alt={"Search"}
                      />
                    </InputAdornment>
                  )
                }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={(): void => {
                  // func.setRouteState(routeType.AddHome);
                  setIsOpenAddPjComptSetDialog(true);
                }}
                // className={classes.addIcon}
              >
                Add
              </Button>
            </div>

            <div>
              <div
                className={classnames(
                  classes.sortingHeader,
                  classes.rowItemComptSetRoot
                )}
              >
                <div className={styles.ComptSetName}>Component Set Name</div>
                <div className={styles.ComptSetDescription}>Description</div>
                <div className={styles.ComptSetLastEditDate}>
                  Last Edit Date
                </div>
                <div className={styles.ComptSetLastEditBy}>Last Edit By</div>
                <div className={classes.functionalIconWrapper}></div>
              </div>

              {comptSetFilteredList.map((componentSet, index) => {
                const { _id, title, description, system } = componentSet;
                return (
                  <div
                    className={classes.rowItemProjectRoot}
                    onClick={(): void => {
                      onClickProjectRow(_id);
                    }}
                    key={index}
                    onMouseEnter={(): void => {
                      setHoverId(_id);
                    }}
                    onMouseLeave={(): void => {
                      setHoverId("");
                    }}
                  >
                    <div className={styles.ComptSetName}>{title}</div>
                    <div className={styles.ComptSetDescription}>
                      {description}
                    </div>
                    <div className={styles.ComptSetLastEditDate}>
                      {system.updatedDate}
                    </div>
                    <div className={styles.ComptSetLastEditBy}>
                      {system.updatedBy}
                    </div>
                    <div className={classes.functionalIconWrapper}>
                      {hoverId === _id && (
                        <Fragment>
                          {/* projectId is the id of this row */}
                          <Tooltip title={"Clone"}>
                            <Copy
                              onClick={(event): void => {
                                event.stopPropagation();
                                setOpenEditPageDialog(true);
                              }}
                            />
                          </Tooltip>
                          <Tooltip title={"Edit"}>
                            <Edit
                              onClick={(event): void => {
                                event.stopPropagation();
                                setSelectedPjComptSet(componentSet);
                                setIsOpenEditPjComptSetDialog(true);
                              }}
                            />
                          </Tooltip>
                          <Tooltip title={"Move"}>
                            <img
                              className={classes.moveIcon}
                              src={Move}
                              alt="move"
                              onClick={(event): void => {
                                event.stopPropagation();
                                setOpenMoveDialog(true);
                              }}
                            />
                          </Tooltip>
                          <Tooltip title={"Delete"}>
                            <Delete
                              onClick={(event): void => {
                                event.stopPropagation();
                                setOpenDeleteDialog(true);
                              }}
                            />
                          </Tooltip>
                        </Fragment>
                      )}
                    </div>
                  </div>
                );
              })}
            </div>
          </Fragment>
        );
      case AddHome:
        return (
          <div className={classes.addContentContainer}>
            <iframe
              src={`${config.webuiIframeUrl}/cmsPreview`}
              width={"100%"}
              height={"100%"}
              id={"webuiIframe"}
              title={"webuiIframe"}
            />
          </div>
        );
      case Setting:
        return <SettingUI />;
      default:
        return null;
    }
  };
  // const { route } = props;
  const projectListForCopy = [
    {
      listTitle: "Project",
      // listItems: projectList.map(item => item.title.en)
      listItems: projectList
    }
  ];
  const openProjectDialogProps = {
    closeDialog: (): void => setOpenAddProjectDialog(false),
    dialogOpen: openAddProjectDialog,
    dialogTopLeftTitle: "Add Project",
    onSubmit: addProjectHandler
  };
  const openCompDialogProps = {
    closeDialog: (): void => setOpenAddProjectDialog(false),
    dialogOpen: openAddProjectDialog,
    dialogTopLeftTitle: "Add Component Set",
    onSubmit: addCompSetHandler,
    namePlaceHolder: "Component Set Name"
  };

  const addDialogProps =
    routeState === AddProject ? openProjectDialogProps : openCompDialogProps;
  return (
    <div className={classes.mainContainer}>
      {openAddPageDialog && (
        <DialogAddPagePopUp
          closeDialog={(): void => {
            setOpenAddPageDialog(false);
          }}
          dialogOpen={openAddPageDialog}
          dialogTopLeftTitle={"Add Page"}
        />
      )}
      {isOpenAddPjComptSetDialog && (
        <DialogAddComptSetDialog
          dialogOpen={isOpenAddPjComptSetDialog}
          closeDialog={(): void => setIsOpenAddPjComptSetDialog(false)}
          comptSetName={comptSetName}
          setComptSetName={setComptSetName}
          comptSetDesc={comptSetDesc}
          setSomptSetDesc={setSomptSetDesc}
          projectId={currentProjectId}
        />
      )}
      {openAddProjectDialog && <DialogAddProjectPopUp {...addDialogProps} />}
      {openDeleteDialog && (
        <ConfirmDialog
          open={openDeleteDialog}
          onConfirm={onDelete}
          onClose={(): void => {
            setOpenDeleteDialog(false);
            setTarget("");
          }}
        />
      )}
      {isOpenEditPjComptSetDialog && (
        <DialogEditPjComptSetPopUp
          closeDialog={(): void => {
            setIsOpenEditPjComptSetDialog(false);
          }}
          dialogOpen={isOpenEditPjComptSetDialog}
          comptSetInfo={selectedPjComptSet}
          projectId={currentProjectId}
          // allPjCOmptSetList={}
          // updateComptSetList={}
        />
      )}
      {/* Display EditPagePopUp */}
      {openEditPageDialog && (
        <DialogEditPagePopUp
          closeDialog={(): void => {
            setOpenEditPageDialog(false);
          }}
          dialogOpen={openEditPageDialog}
          pageInfo={selectedPage}
          allPageList={filteredList}
          updatePageList={setFilteredList}
        />
      )}
      {/* {openCopyDialog && (
        <CopyDialog
          list={projectListForCopy}
          open={openCopyDialog}
          title={"Move To "}
          onConfirm={() => {
            setOpenCopyDialog(false);
            setTarget("");
          }}
          onClose={() => {
            setOpenCopyDialog(false);
            setTarget("");
          }}
        />
        //
      )} */}
      {openMoveDialog && (
        <CopyDialog
          selectedPageId={hoverId}
          list={projectListForCopy}
          open={openMoveDialog}
          title={"Move To "}
          onConfirm={(): void => {
            setOpenMoveDialog(false);
            setTarget("");
          }}
          onClose={(): void => {
            setOpenMoveDialog(false);
            setTarget("");
          }}
        />
      )}
      {error && (
        <ConfirmDialog
          title={"Error"}
          content={errorMsg}
          open={error}
          errorDialog
          onClose={closeErrorDialog}
          onConfirm={(): any => undefined}
        />
      )}

      {returnMainContent()}
    </div>
  );
};

export default MainSection;
