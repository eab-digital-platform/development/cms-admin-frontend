import React from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker
} from "@material-ui/pickers";
import Typography from "@material-ui/core/Typography";

interface TimeSelectorProps {
  title: string;
  timeValue: any;
  changeTimeFunc: Function;
}

const TimeSelector: React.FunctionComponent<TimeSelectorProps> = (
  props: TimeSelectorProps
) => {
  const { title, timeValue, changeTimeFunc } = props;
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Typography>{title}</Typography>
      <KeyboardTimePicker
        fullWidth
        margin="normal"
        id="time-picker"
        // label="Time picker"
        value={timeValue}
        onChange={(time: any): void => changeTimeFunc(time)}
        KeyboardButtonProps={{
          "aria-label": "change time"
        }}
      />
    </MuiPickersUtilsProvider>
  );
};

export default TimeSelector;
