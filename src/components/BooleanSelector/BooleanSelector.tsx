import React, { useState, useEffect } from "react";
import Switch from "@material-ui/core/Switch";

interface BooleanSelectorProps {
  title: string;
  initialValue: boolean;
  propertyKey: string;
  propsState: any;
  fieldOnChange: Function;
  triggerTreeUpdate: Function;
}

const BooleanSelector: React.FunctionComponent<BooleanSelectorProps> = (
  props: BooleanSelectorProps
) => {
  const {
    title,
    initialValue,
    propertyKey,
    propsState,
    fieldOnChange,
    triggerTreeUpdate
  } = props;
  const [selectedValue, setSelectedValue] = useState<boolean>(initialValue);

  useEffect(() => {
    triggerTreeUpdate(propsState);
  }, [propsState]);

  const onChangeHandler = (newValue): void => {
    let isShow = "";
    if (newValue === false) {
      isShow = "false";
    }
    setSelectedValue(newValue);
    const event = {
      currentTarget: {
        value: isShow
      }
    };
    triggerTreeUpdate(propsState);
    fieldOnChange(event, propertyKey);
  };

  return (
    <div>
      <span>{title}</span>
      <Switch
        checked={selectedValue}
        onChange={(): void => onChangeHandler(!selectedValue)}
        value={selectedValue}
        inputProps={{ "aria-label": "secondary checkbox" }}
      />
    </div>
  );
};

export default BooleanSelector;
