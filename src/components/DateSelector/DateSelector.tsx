import React from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Typography from "@material-ui/core/Typography";

interface DateSelectorProps {
  title: string;
  dateValue: Date;
  changeDateFunc: Function;
}

const DateSelector: React.FunctionComponent<DateSelectorProps> = (
  props: DateSelectorProps
) => {
  const { title, dateValue, changeDateFunc } = props;
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Typography>{title}</Typography>
      <KeyboardDatePicker
        fullWidth
        disableToolbar
        variant="inline"
        format="dd/MM/yyyy"
        margin="normal"
        id="date-picker-inline"
        // label="Date picker inline"
        value={dateValue}
        onChange={(date: any): void => changeDateFunc(date)}
        KeyboardButtonProps={{
          "aria-label": "change date"
        }}
      />
    </MuiPickersUtilsProvider>
  );
};

export default DateSelector;
