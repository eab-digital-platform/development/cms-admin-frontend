import React, { useState, useEffect } from "react";
import styles from "./TextAreaBox.module.css";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1)
      }
    }
  })
);

interface TextAreaBoxProps {
  title: string;
  initialValue: string;
  propertyKey: string;
  propsState: any;
  fieldOnChange: Function;
  triggerTreeUpdate: Function;
  closeTextArae: Function;
}

const TextAreaBox: React.FunctionComponent<TextAreaBoxProps> = (
  props: TextAreaBoxProps
) => {
  const classes = useStyles();
  const {
    title,
    initialValue,
    propertyKey,
    propsState,
    fieldOnChange,
    triggerTreeUpdate,
    closeTextArae
  } = props;
  const [textValue, setTextValue] = useState<string>(initialValue);

  useEffect(() => {
    triggerTreeUpdate();
  }, [propsState]);

  const onChangedHandler = (
    e: React.ChangeEvent<HTMLTextAreaElement>
  ): void => {
    setTextValue(e.target.value);
    // const event = {
    //   currentTarget: {
    //     value: e.target.value
    //   }
    // };
    // fieldOnChange(event, propertyKey);
  };

  const onClickedEditHandler = (): void => {
    const event = {
      currentTarget: {
        value: textValue
      }
    };
    fieldOnChange(event, propertyKey);
    triggerTreeUpdate();
    closeTextArae();
  };

  const onClickedCancelHandler = (): void => {
    closeTextArae();
  };

  return (
    <div className={styles.CoverWholePage}>
      <div className={styles.TextAreaBoxContainer}>
        <div>
          <Typography>{title}</Typography>
          <TextareaAutosize
            style={{ width: "500px" }}
            value={textValue}
            onChange={onChangedHandler}
            aria-label="empty textarea"
            placeholder="Type Here ..."
            rowsMin={10}
            rowsMax={20}
          />
        </div>
        <div className={classes.root}>
          <Button
            onClick={(): void => onClickedCancelHandler()}
            variant="contained"
            color="primary"
          >
            Cancel
          </Button>
          <Button
            onClick={(): void => onClickedEditHandler()}
            variant="contained"
            color="primary"
          >
            Edit
          </Button>
        </div>
      </div>
    </div>
  );
};

export default TextAreaBox;
