import React, { Component } from "react";
import styles from "./PageSelector.module.css";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";

import { getPageListOfProject } from "../../api";

interface PageSelectorProps {
  title: string;
  projectId: string;
  initialSelectedPage: any | null;
  selectPageFunc: Function;
}

interface PageSelectorState {
  headerTitle: string;
  isDropdownOpen: boolean;
  pageList: Array<any>;
  selectedPage: any;
}

class PageSelector extends Component<PageSelectorProps, PageSelectorState> {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: this.props.title,
      isDropdownOpen: false,
      pageList: [],
      selectedPage: this.props.initialSelectedPage
    };
  }

  componentDidMount(): void {
    getPageListOfProject(this.props.projectId).then(result => {
      const initialPage = result.filter(
        pageInfo => pageInfo._id === this.props.initialSelectedPage
      );

      this.setState({
        pageList: result,
        selectedPage: initialPage[0]
      });
    });
  }

  toggleDropdown = (): void => {
    this.setState(prevState => {
      return { isDropdownOpen: !prevState.isDropdownOpen };
    });
  };

  selectPage = (pageInfo: any): void => {
    const event = {
      currentTarget: {
        value: pageInfo._id
      }
    };
    this.props.selectPageFunc(event);
    this.setState({
      selectedPage: pageInfo,
      isDropdownOpen: false
    });
  };

  render(): JSX.Element {
    const sortedList = this.state.pageList.sort((prevPage, nextPage) =>
      prevPage.code.toLowerCase() < nextPage.code.toLowerCase() ? -1 : 1
    );

    return (
      <div className={styles.Wrapper}>
        <div
          className={styles.Header}
          onClick={(): void => this.toggleDropdown()}
        >
          <div className={styles.HeaderTitle}>
            <p>
              {this.state.selectedPage
                ? this.state.selectedPage.code
                : this.state.headerTitle}
            </p>
          </div>
          <div className={styles.ArrowIcon}>
            {this.state.isDropdownOpen ? (
              <ArrowDropUpIcon />
            ) : (
              <ArrowDropDownIcon />
            )}
          </div>
        </div>
        {this.state.isDropdownOpen && (
          <ul className={styles.List}>
            {sortedList.map(pageInfo => (
              <li
                key={pageInfo._id}
                className={styles.ListItem}
                onClick={(): void => this.selectPage(pageInfo)}
              >
                {pageInfo.code}
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

export default PageSelector;
