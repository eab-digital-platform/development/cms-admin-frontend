import React, { Component } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Styles from "./ProjectDetailSetting.module.css";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import { TextField } from "@material-ui/core";

interface ProjectDetailSettingProps {
  code: string;
  description: string;
  pageList: Array<any>;
}

interface ProjectDetailSettingState {
  projectName: string;
  projectDescription: string;
  startPage: Array<any>;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      cursor: "pointer"
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  })
);

const classes = useStyles();

class ProjectDetailSetting extends Component<
  ProjectDetailSettingProps,
  ProjectDetailSettingState
> {
  constructor(props) {
    super(props);
    this.state = {
      projectName: this.props.code,
      projectDescription: this.props.description,
      startPage: this.props.pageList
    };
  }

  changeHandler = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as Pick<
      ProjectDetailSettingState,
      keyof ProjectDetailSettingState
    >);
  };

  render(): JSX.Element {
    return (
      <div>
        <h2 className={Styles.ProjDetailSetting}>Project Detail Setting</h2>
        <div>
          <div className={Styles.ProjDetailSettingRow}>
            <h3 className={Styles.ProjDetailSettinLeft}>Project Name</h3>
            <h3 className={Styles.ProjDetailSettinRight}>
              Project Description
            </h3>
          </div>

          <div className={Styles.ProjDetailSettingRow}>
            <div className={Styles.ProjDetailSettinLeft}>
              <TextField
                autoFocus
                fullWidth
                margin="none"
                id="outlined-basic"
                variant="outlined"
                name="projectName"
                value={this.state.projectName}
                // className={classText.root}
                onChange={this.changeHandler}
              />
            </div>
            <div className={Styles.ProjDetailSettinRight}>
              <TextField
                fullWidth
                margin="none"
                id="outlined-basic"
                variant="outlined"
                name="projectDescription"
                value={this.state.projectDescription}
                // className={classText.root}
                onChange={this.changeHandler}
              />
            </div>
          </div>

          <div className={Styles.ProjDetailSettingRow}>
            <div className={Styles.ProjDetailSettinLeft}>
              <FormControl
                fullWidth
                variant="outlined"
                className={classes.formControl}
              >
                <InputLabel htmlFor="start-page">List</InputLabel>
                <Select
                  value={this.state.startPage}
                  inputProps={{
                    name: "startPage",
                    id: "start-page"
                  }}
                >
                  <option value="" />
                  {this.state.startPage.map((page, index) => {
                    return (
                      <option
                        style={{ cursor: "pointer.!important" }}
                        value={page}
                        key={index}
                      >
                        {page}
                      </option>
                    );
                  })}
                </Select>
              </FormControl>
            </div>
            <div className={Styles.ProjDetailSettinRight}></div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProjectDetailSetting;
