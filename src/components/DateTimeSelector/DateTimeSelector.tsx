import React from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker
} from "@material-ui/pickers";
import Typography from "@material-ui/core/Typography";

interface DateTimeSelectorProps {
  title: string;
  dateTimeValue: Date;
  changeDateTimeFunc: Function;
}

const DateTimeSelector: React.FunctionComponent<DateTimeSelectorProps> = (
  props: DateTimeSelectorProps
) => {
  const { title, dateTimeValue, changeDateTimeFunc } = props;
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Typography>{title}</Typography>
      <KeyboardDatePicker
        fullWidth
        disableToolbar
        variant="inline"
        format="dd/MMM/yyyy"
        margin="normal"
        id="date-picker-inline"
        // label="Date picker inline"
        value={dateTimeValue}
        onChange={(date: any): void => changeDateTimeFunc(date)}
        KeyboardButtonProps={{
          "aria-label": "change date"
        }}
      />
      <KeyboardTimePicker
        fullWidth
        margin="normal"
        id="time-picker"
        // label="Time picker"
        value={dateTimeValue}
        onChange={(time: any): void => changeDateTimeFunc(time)}
        KeyboardButtonProps={{
          "aria-label": "change time"
        }}
      />
    </MuiPickersUtilsProvider>
  );
};

export default DateTimeSelector;
