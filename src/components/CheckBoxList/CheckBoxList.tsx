import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex"
    },
    formControl: {
      margin: theme.spacing(3)
    }
  })
);

interface CheckListBoxProps {
  title: string;
  checkList: Array<any>;
  changeCheckList: Function;
}

const CheckListBox: React.FunctionComponent<CheckListBoxProps> = (
  props: CheckListBoxProps
) => {
  const classes = useStyles();
  const { title, checkList, changeCheckList } = props;

  const onChangedHandler = (index: number): void => {
    const newList = [...checkList];
    newList[index].checked = !checkList[index].checked;
    changeCheckList(newList);
  };

  return (
    <div className={classes.root}>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">{title}</FormLabel>
        <FormGroup>
          {checkList.map((checkItem: any, index: number) => {
            return (
              <FormControlLabel
                key={index}
                control={
                  <Checkbox
                    checked={checkItem.checked as boolean}
                    onChange={(): void => onChangedHandler(index)}
                    value={checkItem.text}
                  />
                }
                label={checkItem.text}
              />
            );
          })}
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default CheckListBox;
