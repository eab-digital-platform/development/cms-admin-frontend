import React, { Fragment, ChangeEvent } from "react";
import { Button, TextField } from "@material-ui/core";
import ConfirmDialog from "../../common/ConfirmDialog";
import Styles from "./EnvironmentVariableSetting.module.css";
import { ReactComponent as Delete } from "../../assets/Delete.svg";

interface EnvValPairProps {
  envName: string;
  envValue: string;
  onClicked: Function;
  onChanged: Function;
}

interface EnvValPairState {
  isOpenConfirmDialog: boolean;
}

class EnvValPair extends React.Component<EnvValPairProps, EnvValPairState> {
  constructor(props) {
    super(props);
    this.state = {
      isOpenConfirmDialog: false
    };
  }

  openConfirmDialogHandler = (): void => {
    this.setState({ isOpenConfirmDialog: true });
  };

  confirmActionHandler = (): void => {
    this.setState({ isOpenConfirmDialog: false });
    this.props.onClicked();
  };

  render(): JSX.Element {
    return (
      <Fragment>
        {this.state.isOpenConfirmDialog && (
          <ConfirmDialog
            open={this.state.isOpenConfirmDialog}
            onConfirm={this.confirmActionHandler}
            onClose={(): void => {
              this.setState({ isOpenConfirmDialog: false });
            }}
          />
        )}

        <div className={Styles.EnvValueSettingRow}>
          <div className={Styles.EnvValueSettingLeft}>
            <TextField
              fullWidth
              margin="none"
              id="outlined-basic"
              variant="outlined"
              placeholder={"Name"}
              name="name"
              value={this.props.envName}
              onChange={(e: ChangeEvent<HTMLInputElement>): void =>
                this.props.onChanged(e)
              }
            />
          </div>

          <div className={Styles.EnvValueSettingRight}>
            <TextField
              fullWidth
              margin="none"
              id="outlined-basic"
              variant="outlined"
              placeholder={"Value"}
              name="value"
              value={this.props.envValue}
              onChange={(e: ChangeEvent<HTMLInputElement>): void =>
                this.props.onChanged(e)
              }
            />
          </div>

          <Delete onClick={this.openConfirmDialogHandler} />
        </div>
      </Fragment>
    );
  }
}

interface EnvironmentVariableSettingProps {
  envValPairList: Array<any>;
  deletePair: Function;
  onChanged: Function;
  addNew: Function;
}

interface EnvironmentVariableSettingState {
  pairList: Array<any>;
}

class EnvironmentVariableSetting extends React.Component<
  EnvironmentVariableSettingProps,
  EnvironmentVariableSettingState
> {
  constructor(props) {
    super(props);
    const { envValPairList } = props;
    this.state = {
      pairList: envValPairList
    };
  }

  deleteHandler = (index: number): void => {
    const updatedList = [...this.state.pairList];
    updatedList.splice(index, 1);
    this.setState({ pairList: updatedList });
  };

  addEnvironmentSettingPair = (): void => {
    const updatedList = [...this.state.pairList];
    updatedList.push({ name: "", value: "" });
    this.setState({ pairList: updatedList });
  };

  render(): JSX.Element {
    return (
      <div>
        <h2 className={Styles.EnvValSetting}>Environment Variable Setting</h2>
        <div>
          <div className={Styles.EnvValueSettingRow}>
            <h3 className={Styles.EnvValueSettingLeft}>Name</h3>
            <h3 className={Styles.EnvValueSettingRight}>Value</h3>
          </div>

          {this.props.envValPairList.map((pair, index) => {
            return (
              <EnvValPair
                key={index}
                envName={pair.name}
                envValue={pair.value}
                onClicked={(): void => this.props.deletePair(index)}
                onChanged={(e: ChangeEvent<HTMLInputElement>): void =>
                  this.props.onChanged(e, index)
                }
              />
            );
          })}

          <Button
            onClick={(): void => this.props.addNew()}
            variant="contained"
            color="primary"
          >
            Add
          </Button>
        </div>
      </div>
    );
  }
}

export default EnvironmentVariableSetting;
