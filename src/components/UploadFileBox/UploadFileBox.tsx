import React, { useState, useContext, useEffect } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { uploadFileToPageMaster } from "../../api";
import { RouteContext } from "../../contextComponent";
import styles from "./UploadFileBox.module.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1)
      }
    }
  })
);

interface UploadFileBoxProps {
  title: string;
  initialValue: any;
  propertyKey: string;
  propsState: any;
  fieldOnChange: Function;
  triggerTreeUpdate: Function;
  cancelClickFunc: Function;
}

const UploadFileBox: React.FunctionComponent<UploadFileBoxProps> = (
  props: UploadFileBoxProps
) => {
  const classes = useStyles();
  const {
    title,
    initialValue,
    propertyKey,
    propsState,
    fieldOnChange,
    triggerTreeUpdate,
    cancelClickFunc
  } = props;
  const { state } = useContext(RouteContext);
  const { currentPageId } = state;
  const [selectedFile, setSelectedFile] = useState<any>(initialValue);
  const [previewFile, setPreviewFile] = useState<any>(initialValue);

  useEffect(() => {
    triggerTreeUpdate(propsState);
    return triggerTreeUpdate(propsState);
  }, [propsState]);

  const onChangedHandler = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (e.target.files !== null) {
      setSelectedFile(e.target.files[0]);
      setPreviewFile(URL.createObjectURL(e.target.files[0]));
    }
  };

  const uploadHandler = (): void => {
    const data = new FormData();
    data.append("file", selectedFile);
    // data.append("objectServiceId", {})
    uploadFileToPageMaster(currentPageId, data)
      .then((data: any) => {
        console.log("[UploadFileBox.tsx] data", data);
        const event = {
          currentTarget: {
            value: data.url
          }
        };
        fieldOnChange(event, propertyKey);
        cancelClickFunc();
      })
      .catch(err => {
        console.log(err);
        cancelClickFunc();
      });
  };

  const cancelHandler = (): void => {
    cancelClickFunc();
  };

  return (
    <div className={styles.EmptyWrapper}>
      <div className={styles.UploadFileBoxContainer}>
        <Typography className={styles.Title}>{title}</Typography>
        <div className={styles.ImagePreiveBox}>
          <img src={previewFile} />
          <input
            type="file"
            name="file"
            onChange={onChangedHandler}
            placeholder="Drag Here"
          />
          <p>Drag and Drop</p>
        </div>

        <div className={styles.BottomWrapper}>
          <div className={classes.root}>
            <Button
              variant="contained"
              color="secondary"
              onClick={cancelHandler}
            >
              Cancel
            </Button>
            <Button variant="contained" color="primary" onClick={uploadHandler}>
              Upload
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UploadFileBox;
