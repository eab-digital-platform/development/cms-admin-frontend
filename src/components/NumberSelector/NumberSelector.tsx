import React, { FunctionComponent } from "react";
import classnames from "classnames";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Typography from "@material-ui/core/Typography";

interface NumberSelectorProps {
  title: string;
  value: number;
  changeValueFun: Function;
}

const NumberSelector: FunctionComponent<NumberSelectorProps> = (
  props: NumberSelectorProps
) => {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        display: "flex",
        flexWrap: "wrap"
      },
      margin: {
        margin: theme.spacing(1)
      },
      withoutLabel: {
        marginTop: theme.spacing(3)
      },
      textField: {
        width: "100%"
      },
      padding: {
        padding: "10px"
      }
    })
  );
  const classes = useStyles();
  const { title, value, changeValueFun } = props;
  return (
    <FormControl
      className={classnames(
        classes.margin,
        classes.withoutLabel,
        classes.textField
      )}
    >
      {/* <FormHelperText id="standard-weight-helper-text" variant="outlined"> */}
      <Typography className={classes.padding}>{title}</Typography>
      {/* </FormHelperText> */}
      <OutlinedInput
        className={classes.padding}
        id="standard-adornment-weight"
        value={value}
        onChange={(): void => changeValueFun()}
        aria-describedby="standard-weight-helper-text"
        inputProps={{
          "aria-label": "number"
        }}
        labelWidth={0}
      />
    </FormControl>
  );
};

export default NumberSelector;
