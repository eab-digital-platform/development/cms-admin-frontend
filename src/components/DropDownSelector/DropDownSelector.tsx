import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      marginLeft: 0,
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  })
);

interface DropDownSelectorProps {
  title: string;
  dropdownList: Array<any>;
  initialValue: string;
  propertyKey: string;
  propsState: any;
  fieldOnChange: Function;
  triggerTreeUpdate: Function;
}

const DropDownSelector: React.FunctionComponent<DropDownSelectorProps> = (
  props: DropDownSelectorProps
) => {
  const classes = useStyles();
  const inputLabel = React.useRef<HTMLLabelElement>(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    setLabelWidth(inputLabel.current!.offsetWidth);
  }, []);

  const {
    title,
    dropdownList,
    initialValue,
    propertyKey,
    propsState,
    fieldOnChange,
    triggerTreeUpdate
  } = props;
  const [selectedValue, setSelectedValue] = useState<string>(initialValue);

  useEffect(() => {
    triggerTreeUpdate();
  }, [propsState]);

  const onChangedHanlder = (e): void => {
    setSelectedValue(e.target.value);
    const event = {
      currentTarget: {
        value: e.target.value
      }
    };
    fieldOnChange(event, propertyKey);
  };

  const onCloseHandler = (): void => {
    triggerTreeUpdate();
  };

  return (
    <FormControl fullWidth variant="outlined" className={classes.formControl}>
      <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
        {title}
      </InputLabel>
      <Select
        fullWidth
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        value={selectedValue}
        onChange={onChangedHanlder}
        onClose={onCloseHandler}
        labelWidth={labelWidth}
      >
        {dropdownList.map((item: any, index: number) => {
          return (
            <MenuItem key={index} value={item}>
              {item}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};

export default DropDownSelector;
