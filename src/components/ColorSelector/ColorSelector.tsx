import React, { Component } from "react";
import styles from "./ColorSelector.module.css";
import ColorPicker from "material-ui-color-picker";
// import Typography from "@material-ui/core/Typography";

interface ColorSelectorProps {
  title: string;
  initialSelectedColor: string;
  selectColorFunc: Function;
}

interface ColorSelectorState {
  headerTitle: string;
  selectedColor: string;
}

class ColorSelector extends Component<ColorSelectorProps, ColorSelectorState> {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: this.props.title,
      selectedColor: this.props.initialSelectedColor
    };
  }

  selectColor = (color: string): void => {
    const event = {
      currentTarget: {
        value: color
      }
    };
    this.props.selectColorFunc(event);
    this.setState({ selectedColor: color });
  };

  render(): JSX.Element {
    return (
      <ColorPicker
        className={styles.Wrapper}
        fullWidth
        label={this.state.selectedColor}
        value={this.state.selectedColor}
        onChange={(color: string): void => this.selectColor(color)}
      />
    );
  }
}

export default ColorSelector;

// {/* <div className={styles.Wrapper}> */}
//  {/* <Typography>{this.state.headerTitle}</Typography> */}
// </div>;
