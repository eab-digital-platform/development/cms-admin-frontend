import React, { FunctionComponent, useState, useContext } from "react";
import { TextField, Button } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";

import { DialogBasicProps } from "./type/interfaceCommon";
import { RouteContext } from "./contextComponent";
import { updateProjectComptSet, getComponentSetList } from "./api";

interface DialogEditComptSetPopUpProps extends DialogBasicProps {
  comptSetInfo: any;
  projectId: string;
}

const DialogEditPjComptSetPopUp: FunctionComponent<DialogEditComptSetPopUpProps> = (
  props: DialogEditComptSetPopUpProps
) => {
  const { func } = useContext(RouteContext);
  const { setPjComptSetList } = func;
  const { dialogOpen, closeDialog, comptSetInfo, projectId } = props;
  const [comptSetName, setComptSetName] = useState<string>(comptSetInfo.title);
  const [comptSetDesc, setComptSetDesc] = useState<string>(
    comptSetInfo.description
  );

  const clickCnacelBtnHandler = (): void => {
    closeDialog();
  };

  const clickEditBtnHandler = (): void => {
    const data = {
      title: comptSetName,
      description: comptSetDesc,
      cmsComponentType: comptSetInfo.cmsComponentType,
      style: comptSetInfo.style,
      cmsProps: comptSetInfo.cmsProps,
      muiProps: comptSetInfo.muiProps,
      items: comptSetInfo.items
    };
    updateProjectComptSet(projectId, comptSetInfo._id, data).then(() => {
      getComponentSetList(projectId).then(data => {
        setPjComptSetList(data);
        closeDialog();
      });
    });
  };

  const changeComptSetNameHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setComptSetName(e.target.value);
  };

  const changeComptSetDescHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setComptSetDesc(e.target.value);
  };

  return (
    <Dialog open={dialogOpen} onClose={closeDialog} fullWidth maxWidth={"sm"}>
      <DialogTitle>Edit Component Set</DialogTitle>
      <DialogContent dividers>
        <TextField
          autoFocus
          fullWidth
          // className={classes.pageNmae}
          variant="outlined"
          value={comptSetName}
          onChange={changeComptSetNameHandler}
          placeholder={"Page Name"}
        />
        <TextField
          fullWidth
          variant="outlined"
          value={comptSetDesc}
          onChange={changeComptSetDescHandler}
          placeholder={"Description"}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={clickCnacelBtnHandler}
          variant="contained"
          color="primary"
        >
          Cancel
        </Button>
        <Button
          disabled={comptSetName === "" || comptSetDesc === "" ? true : false}
          onClick={clickEditBtnHandler}
          variant="contained"
          color="primary"
        >
          Edit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogEditPjComptSetPopUp;
