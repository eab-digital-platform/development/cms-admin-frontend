import { createContext } from "react";

export const RouteContext = createContext({
  state: {} as any,
  func: {} as any
});
