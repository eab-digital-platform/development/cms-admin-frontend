import React, {
  FunctionComponent,
  useContext,
  Fragment,
  useState
} from "react";
import { RouteContext } from "./contextComponent";
import { makeStyles } from "@material-ui/core/styles";
import { routeType } from "./type/enum";
import TreeWrapper from "./treeContent";
import classnames from "classnames";
import { LeftMenuElement } from "./type/enum";
import { TreeDataStructure, MenuItem } from "./type/interfaceCommon";
import { findTitleById, findCodeById } from "./common/commonFunction";
import GoBack from "./assets/GoBack.png";
import ConfirmDialog from "./common/ConfirmDialog";
// import {
//   getOrganizationAllProjectList,
//   getOrganizationAllPageList,
//   getOrganizationAllComponentSetList
// } from "./api";

interface LeftMenuSectionProps {
  arrayOfItems: MenuItem[];
  selectedElement: MenuItem;
  onSelectHandler: (selectedElement: MenuItem) => void;
}

// const [selectedItem, setSelectedItem] = useState<MenuItem>({ id: "" });

const LeftMenuSection: FunctionComponent<LeftMenuSectionProps> = (
  props: LeftMenuSectionProps
) => {
  const { arrayOfItems, selectedElement, onSelectHandler } = props;
  const useStyles = makeStyles({
    leftMenuContainer: {
      fontSize: "var(--Afont-M)",
      width: "300px",
      padding: "var(--Apadding-XXS) var(--Apadding-XXS) var(--Apadding-XXS) 0",
      overflowX: "hidden",
      overflowY: "auto"
      // height: "calc(100% - 85px)"
      // height: "100%"
    },
    menuItem: {
      // backgroundColor: "lightGreen",
      // border: "1px solid",
      lineHeight: "50px",
      textAlign: "left",
      padding: "0 var(--Apadding-56)",
      cursor: "pointer"
    },
    projectTitleItem: {
      lineHeight: "80px",
      textAlign: "left",
      paddingLeft: "var(--Afont-M)",
      cursor: "pointer",
      color: "var(--Aprimary-color-light1)",
      fontSize: "var(--Afont-M)",
      fontWeight: "bold",
      alignItems: "center",
      display: "flex"
    },
    selectedItem: {
      color: "var(--Aprimary-color-light1)",
      borderLeft: "var(--Aborder-L) var(--Aprimary-color-light1) solid",
      paddingLeft: "50px",
      fontWeight: "bold"
    },
    generalTitle: {
      padding: "var(--Apadding-XXM) var(--Apadding-M) var(--Apadding-XXXXS)",
      fontSize: "var(--Afont-M)",
      cursor: "pointer",
      lineHeight: "40px"
    },
    withShadow: {
      boxShadow: "0px var(--Aborder-S) var(--Aborder-M) var(--Ashadow-general)"
    },
    organization: {
      fontSize: "var(--Afont-M)",
      fontWeight: "bold",
      cursor: "default",
      lineHeight: "80px"
    },
    goBackIcon: {
      margin: "var(--Apadding-XXXS)",
      height: "24px",
      width: "24px"
    },
    overflowText: {
      textOverflow: "ellipsis",
      overflow: "hidden",
      whiteSpace: "nowrap"
    }
  });
  const classes = useStyles();
  const { state, func } = useContext(RouteContext);
  const {
    currentProjectId,
    currentPageId,
    routeState,
    treeDataState,
    pageList,
    projectList,
    isAskSave
  } = state;
  const { setTreeDataState, setRouteState, setIsAskSave } = func;
  const [isOpenConfirmDialog, setIsOpenConfirmDialog] = useState<boolean>(
    false
  );
  const { IOrgan } = LeftMenuElement;
  let projectCode = "";
  if (projectList && projectList.length > 0 && currentProjectId) {
    projectCode = findTitleById(projectList, currentProjectId) || "";
  }

  const setTreeData = (item: TreeDataStructure[]): void => {
    setTreeDataState(item);
  };

  const returnLeftMenu = (): JSX.Element | undefined | null => {
    const { Index, AddHome, AddProject, Setting, AddComponentSet } = routeType;
    switch (routeState) {
      case Index:
      case Setting:
      case AddComponentSet:
        return (
          <Fragment>
            {currentProjectId && (
              <div
                className={classes.projectTitleItem}
                onClick={(): void => setRouteState(AddProject)}
              >
                <img
                  src={GoBack}
                  alt={"GoBack"}
                  className={classes.goBackIcon}
                />
                <div className={classes.overflowText}>{projectCode}</div>
              </div>
            )}
            {arrayOfItems.map((item, index) => {
              const { displayName, id } = item;
              const displayText = displayName ? displayName : id;
              if (id === IOrgan) {
                return (
                  <div
                    key={index}
                    className={classnames(
                      classes.menuItem,
                      classes.organization,
                      classes.overflowText
                    )}
                  >
                    {displayName}
                  </div>
                );
              }
              return [
                <div
                  className={`${
                    selectedElement.id === id ? classes.selectedItem : ""
                  } ${classes.menuItem}`}
                  key={index}
                  onClick={(): void => onSelectHandler(item)}
                  id={`leftMenu_${
                    displayName ? displayName.replace(/\s/g, "") : ""
                  }`}
                >
                  {displayText}
                </div>
              ];
            })}
          </Fragment>
        );
      case AddHome:
        let pageCode = "";
        if (pageList && pageList.length > 0 && currentPageId) {
          pageCode = findCodeById(pageList, currentPageId);
        }
        return (
          <Fragment>
            {isOpenConfirmDialog && (
              <ConfirmDialog
                title={"Page Save"}
                content={"Are you sure go back without save the change?"}
                open={isOpenConfirmDialog}
                onConfirm={(): void => {
                  setIsAskSave(false);
                  setIsOpenConfirmDialog(false);
                  setRouteState(Index);
                }}
                onClose={(): void => setIsOpenConfirmDialog(false)}
              />
            )}
            <div
              className={classnames(
                classes.generalTitle,
                classes.projectTitleItem
              )}
              onClick={(): void => {
                if (isAskSave === false) {
                  setRouteState(Index);
                } else {
                  setIsOpenConfirmDialog(true);
                }
              }}
            >
              <img src={GoBack} alt={"GoBack"} className={classes.goBackIcon} />
              <div className={classes.overflowText}>{pageCode}</div>
            </div>
            <TreeWrapper
              treeData={treeDataState}
              // addItemToTree={onAddButtonClick}
              setTreeData={setTreeData}
            />
          </Fragment>
        );
      case AddProject:
        return (
          <Fragment>
            {arrayOfItems.map((item, index) => {
              const { displayName, id } = item;
              const displayText = displayName ? displayName : id;
              if (id === IOrgan) {
                return (
                  <div
                    key={index}
                    className={classnames(
                      classes.menuItem,
                      classes.organization,
                      classes.overflowText
                    )}
                  >
                    {displayName}
                  </div>
                );
              } else {
                return [
                  <div
                    className={`${
                      selectedElement.id === id ? classes.selectedItem : ""
                    } ${classes.menuItem}`}
                    key={index}
                    onClick={(): void => onSelectHandler(item)}
                    id={`leftMenu_${
                      displayName ? displayName.replace(/\s/g, "") : ""
                    }`}
                  >
                    {displayText}
                  </div>
                ];
              }
            })}
          </Fragment>
        );
      default:
        return <span>I a here</span>;
    }
  };
  return (
    <div className={classnames(classes.leftMenuContainer, classes.withShadow)}>
      {returnLeftMenu()}
    </div>
  );
};

export default LeftMenuSection;
