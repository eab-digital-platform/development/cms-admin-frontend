import React, { FunctionComponent, useState } from "react";
import { TextField, Button } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";

import { savePage } from "./api";
import { DialogBasicProps } from "./type/interfaceCommon";

interface TDialogEditPagePopUp extends DialogBasicProps {
  allPageList: any;
  updatePageList: any;
}

const DialogEditPagePopUp: FunctionComponent<TDialogEditPagePopUp> = (
  props: TDialogEditPagePopUp
) => {
  // state use in this component
  const { dialogOpen, closeDialog, pageInfo, allPageList } = props;
  const [pageName, setPageName] = useState(pageInfo.code);
  const [pageDescription, setPageDescription] = useState(
    pageInfo.description.en
  );

  const cnacelHandler = (): void => {
    closeDialog();
  };

  const editHandler = (): void => {
    const data = {
      code: pageName,
      description: {
        en: pageDescription
      }
    };
    savePage(pageInfo.uId, data)
      .then(() => {
        const newPageList = [...allPageList];
        // const newPageList = newPageList.filter((page) => {
        // 	if (page.uId = pageInfo.uId) {
        // 		return
        // 	}
        // })
        const atIndex = newPageList.indexOf(pageInfo);
        newPageList[atIndex].code = pageName;
        newPageList[atIndex].description.en = pageDescription;
        closeDialog();
      })
      .catch(err => {
        console.log(err);
      });
  };

  const changePageNameHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setPageName(e.target.value);
  };

  const changePageDescriptionHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setPageDescription(e.target.value);
  };

  return (
    <Dialog open={dialogOpen} onClose={closeDialog} fullWidth maxWidth={"sm"}>
      <DialogTitle>
        <DialogTitle>Edit</DialogTitle>
      </DialogTitle>
      <DialogContent dividers>
        <TextField
          autoFocus
          fullWidth
          // className={classes.pageNmae}
          variant="outlined"
          value={pageName}
          onChange={changePageNameHandler}
          placeholder={"Page Name"}
        />
        <TextField
          fullWidth
          variant="outlined"
          value={pageDescription}
          onChange={changePageDescriptionHandler}
          placeholder={"Description"}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={cnacelHandler} variant="contained" color="primary">
          Cancel
        </Button>
        <Button onClick={editHandler} variant="contained" color="primary">
          Edit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogEditPagePopUp;
