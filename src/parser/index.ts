import _ from "lodash";
import { v4 as uuidv4 } from "uuid";
import {
  PageInterface,
  Feb27ProjectInterface,
  PropertyItemStructure,
  Feb24TreeStructureDefinedByTeam,
  ComponentMasterStructure, 
  ComponentSetStructure
} from "../type/interfaceCommon";

export const parseObject = (data: any): Record<string, any> => {
  return _.isObject(data) && !Array.isArray(data) ? data : {};
};

export const parseArray = (data: any): any[] => {
  return _.isArray(data) ? data : [];
};

export const parseBoolean = (data: any): boolean => {
  return _.isBoolean(data) ? data : true;
};

export const parseNumber = (data: any): number => {
  return _.isNumber(data) ? data : 0;
};

export const parseString = (data: any): string => {
  return _.isString(data) ? data : "";
};

export const convertTheJsonToAdd = (data: any): any => {
  const parseItem = parseObject(data);
  const displayName = parseObject(parseItem.displayName);
  const returnJson = {
    _id: parseItem._id || "",
    cmsComponentType: parseItem.cmsComponentType,
    key: parseItem.key || `${parseItem.cmsComponentType}_${uuidv4()}`,
    style: {},
    cmsProps: {},
    muiProps: {},
    items: [],
    displayName: {
      en: parseString(displayName.en)
      //   TODO Add for zh-hant
    }
  };
  if (parseItem.items) {
    returnJson.items = parseItem.items; //TODO:
  } else {
    delete returnJson["items"];
  }
  return returnJson;
};

export const parseNewTreeStructure = (
  item: any
): Feb24TreeStructureDefinedByTeam => {
  const parseItem = parseObject(item);
  const cmsComponentType = parseString(parseItem.cmsComponentType);
  const displayName = parseObject(parseItem.displayName);
  let child = parseItem.items;
  if (child && child.length > 0) {
    child = child.map(ele => {
      return parseNewTreeStructure(ele);
    });
  }
  const returnJson: Feb24TreeStructureDefinedByTeam = {
    key: `${cmsComponentType}_${uuidv4()}`,
    cmsComponentType: cmsComponentType,
    style: parseObject(parseItem.style),
    muiProps: parseObject(parseItem.muiProps),
    cmsProps: parseObject(parseItem.cmsProps),
    displayName: {
      en: parseString(displayName.en)
      //   TODO Add for zh-hant
    }
  };
  if (child) {
    returnJson.items = child;
  }
  if (parseItem._id) {
    returnJson._id = parseItem._id;
  }
  return returnJson;
};

export const parseNewTreeStructureList = (
  data: any
): Feb24TreeStructureDefinedByTeam[] | [] => {
  const newData = parseArray(data);
  return newData.map(item => {
    return parseNewTreeStructure(item);
  });
};

export const parseComponentMaster = (item: any): ComponentMasterStructure => {
  const parseItem = parseObject(item);
  const displayName = parseObject(parseItem.displayName);
  const returnJson = {
    _id: parseString(parseItem._id),
    cmsComponentType: parseString(parseItem.cmsComponentType),
    style: {},
    cmsProps: {},
    muiProps: {},
    displayName: {
      en: parseString(displayName.en)
      //   TODO Add for zh-hant
    },
    imageUrl: parseString(parseItem.imageUrl),
    items: []
  };
  if (parseItem.items) {
    returnJson.items = parseItem.items; //TODO:
  } else {
    delete returnJson["items"];
  }

  return returnJson;
};

export const parseComponentMasterList = (
  data: any
): ComponentMasterStructure[] | [] => {
  const newData = parseArray(data);
  return newData.map(item => {
    return parseComponentMaster(item);
  });
};

export const parsePropertyStructure = (item: any): PropertyItemStructure => {
  const parseItem = parseObject(item);
  const style = parseArray(parseItem.style);
  const muiProps = parseArray(parseItem.muiProps);
  const cmsProps = parseArray(parseItem.cmsProps);
  const returnJson: PropertyItemStructure = {
    cmsComponentType: parseString(parseItem.cmsComponentType),
    imageUrl: parseString(parseItem.imageUrl),
    style: style.map(item => {
      return {
        key: parseString(item.key),
        value: parseObject(item.value)
      };
    }),
    muiProps: muiProps.map(item => {
      return {
        key: parseString(item.key),
        value: parseObject(item.value)
      };
    }),
    cmsProps: cmsProps.map(item => {
      return {
        key: parseString(item.key),
        value: parseObject(item.value)
      };
    })
  };
  if (parseItem._id) {
    returnJson._id = parseItem._id;
  }
  return returnJson;
};

export const parsePropertyStructureList = (
  data: any
): PropertyItemStructure[] | [] => {
  const newData = parseArray(data);
  return newData.map(item => {
    return parsePropertyStructure(item);
  });
};

export const parsePage = (item: any): PageInterface => {
  const parseItem = parseObject(item);
  const system = parseObject(parseItem.system);
  const displayName = parseObject(parseItem.displayName);
  const description = parseObject(parseItem.description);
  const {
    userComponent,
    group,
    createdDate,
    createdBy,
    updatedDate,
    updatedBy
  } = system;

  const returnJson: PageInterface = {
    uId: parseString(parseItem._id) || parseString(parseItem.uId),
    type: parseString(parseItem.type),
    // keys: parseItem.keys || `${parseItem.type}_${new Date().valueOf()}`,
    system: {
      userComponent: parseBoolean(userComponent),
      group: parseString(group),
      createdDate: parseString(createdDate),
      createdBy: parseString(createdBy),
      updatedDate: parseString(updatedDate),
      updatedBy: parseString(updatedBy)
    },
    code: parseString(parseItem.code),
    level: parseString(parseItem.level),
    displayName: {
      en: parseString(displayName.en)
      //   TODO Add for zh-hant
    },
    description: {
      en: parseString(description.en)
      //   TODO Add for zh-hant
    }
  };
  return returnJson;
};

export const parsePageList = (data: any): PageInterface[] | [] => {
  const newData = parseArray(data);
  return newData.map(item => {
    return parsePage(item);
  });
};

export const parseComponentSetList = (data: any): ComponentSetStructure[] | []  => {
  const newData = parseArray(data);
  return newData;
};

export const parseProject = (item: any): Feb27ProjectInterface => {
  const parseItem = parseObject(item);
  const system = parseObject(parseItem.system);
  const description = parseObject(parseItem.description);
  const settings = parseObject(parseItem.settings);
  const { theme, sourceHosts = parseArray(settings.sourceHosts) } = settings;
  const title = parseObject(parseItem.title);
  const { createdDate, createdBy, updatedDate, updatedBy } = system;
  return {
    system: {
      createdDate: parseString(createdDate),
      createdBy: parseString(createdBy),
      updatedDate: parseString(updatedDate),
      updatedBy: parseString(updatedBy)
    },
    uId: parseString(parseItem._id) || parseString(parseItem.uId),
    title: {
      en: parseString(title.en)
    },
    settings: {
      theme: parseString(theme),
      sourceHosts: sourceHosts.map(item => {
        return {
          name: parseString(item.name),
          value: parseString(item.value)
        };
      })
    },
    description: {
      en: parseString(description.en)
      //   TODO Add for zh-hant
    }
  };
};

export const parseProjectList = (data: any): Feb27ProjectInterface[] | [] => {
  const newData = parseArray(data);
  return newData.map(item => {
    return parseProject(item);
  });
};

// export const parseOldJson = data => {
//   const loopRemove = data => {
//     for (const propName in data) {
//       if (data[propName] && typeof data[propName] === "object") {
//         const obj = data[propName];
//         loopRemove(obj);
//       } else if (data[propName] === null) {
//         delete data[propName];
//       }
//     }
//   };

//   let rows = data.rows || [];
//   if (rows.length > 0) {
//     rows = rows.map(item => {
//       let child = item.columns || item.rows || item.items || [];
//       if (child.length > 1) {
//         child = child.map(ele => {
//           return parseOldJson(ele);
//         });
//       }
//       return {
//         type: "gridRowItems",
//         level: "AXA",
//         cssClass: "Basic",
//         pageOrComponent: "Component",
//         keys: `gridRowItems_${Math.round(Math.random() * 10000)}`,
//         system: {},
//         displayName: {
//           en: "gridRowItems"
//         },
//         cmsProperty: {},
//         property: {
//           items: child
//         },
//         style: {},
//         structure: {
//           span: item.span || null
//         }
//       };
//     });
//   }

//   let columns = data.columns || [];
//   if (columns.length > 0) {
//     columns = columns.map(item => {
//       let child = item.columns || item.rows || item.items || [];
//       if (child.length > 1) {
//         child = child.map(ele => {
//           return parseOldJson(ele);
//         });
//       }
//       return {
//         type: "gridColumnItems",
//         level: "AXA",
//         cssClass: "Basic",
//         pageOrComponent: "Component",
//         keys: `gridColumnItems_${Math.round(Math.random() * 10000)}`,
//         system: {},
//         displayName: {
//           en: "gridColumnItems"
//         },
//         cmsProperty: {},
//         property: {
//           items: child
//         },
//         style: {},
//         structure: {
//           span: item.span || null
//         }
//       };
//     });
//   }

//   let child = data.items || data.elements || [];
//   if (child.length > 0) {
//     child = child.map(item => {
//       return parseOldJson(item);
//     });
//   }

//   let real = [];
//   if (child.length > 0) {
//     real = child;
//   } else if (rows.length > 0) {
//     real = rows;
//   } else if (columns.length > 0) {
//     real = columns;
//   }
//   if (data.name && typeof data.name === "object") {
//     data.name = data.name.en;
//   }
//   const newData = {
//     type: data.type || "untitle",
//     level: "AXA",
//     keys: data.keys || `${data.type}_${Math.round(Math.random() * 10000)}`,
//     cssClass: data.css_class || null,
//     pageOrComponent: data.pageOrComponent || null,
//     uId: data.id || null,
//     code: data.type || null,
//     description: {
//       en: data.name || null
//     },
//     dataUrl: data.data_url || null,
//     style: {
//       ...data.style,
//       width: data.width || null,
//       height: data.height || null,
//       background: data.background || null,
//       backgroundColor: data.backgroundColor || null,
//       spacing: data.spacing || null,
//       direction: data.direction || null,
//       justify: data.justify || null,
//       alignItems: data.alignItems || null,
//       position: data.position || null,
//       padding: data.padding || null,
//       margin: data.margin || null,
//       display: data.display || null,
//       flex: data.flex || null
//     },
//     system: {
//       createdDate: data.create_date || null,
//       createdBy: data.create_by || null,
//       updatedData: data.update_date || null,
//       updatedBy: data.update_by || null,
//       version: data.version || null
//     },
//     displayName: {
//       en: data.text || data.name || data.iconName || data.type || "untitle"
//     },
//     structure: {
//       actionUrl: data.action_URL || null,
//       align: data.align || null,
//       valign: data.valign || null,
//       iconName: data.iconName || null,
//       span: data.span || null
//     },
//     cmsProperty: {
//       style: [
//         data.width
//           ? {
//               key: "width",
//               value: {
//                 title: "Width",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.height
//           ? {
//               key: "height",
//               value: {
//                 title: "height",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.background
//           ? {
//               key: "background",
//               value: {
//                 title: "background",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.backgroundColor
//           ? {
//               key: "backgroundColor",
//               value: {
//                 title: "backgroundColor",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.spacing
//           ? {
//               key: "spacing",
//               value: {
//                 title: "spacing",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.direction
//           ? {
//               key: "direction",
//               value: {
//                 title: "direction",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.justify
//           ? {
//               key: "justify",
//               value: {
//                 title: "justify",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.alignItems
//           ? {
//               key: "alignItems",
//               value: {
//                 title: "alignItems",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.position
//           ? {
//               key: "position",
//               value: {
//                 title: "position",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.padding
//           ? {
//               key: "padding",
//               value: {
//                 title: "padding",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.margin
//           ? {
//               key: "margin",
//               value: {
//                 title: "margin",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.display
//           ? {
//               key: "display",
//               value: {
//                 title: "display",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null,
//         data.flex
//           ? {
//               key: "flex",
//               value: {
//                 title: "flex",
//                 input: "string",
//                 length: 3,
//                 checking: "#px-percentage-regex",
//                 default: "100%",
//                 mandatory: false
//               }
//             }
//           : null
//       ]
//     },
//     property: {
//       items: real
//     }
//   };
//   loopRemove(newData);
//   const newFiltered = newData.cmsProperty.style.filter(el => el != null);
//   newData.cmsProperty.style = newFiltered;
//   return newData;
// };

// export const parseAddPageList = data => {
//   const newData = {
//     type: data.type || "untitle",
//     level: parseString(data.level),
//     keys: data.keys || `${data.type}_${Math.round(Math.random() * 10000)}`,
//     cssClass: data.css_class,
//     pageOrComponent: parseString(data.pageOrComponent),
//     uId: parseString(data._id),
//     code: parseString(data.type),
//     description: {
//       en: data.name || null
//     },
//     dataUrl: parseString(data.data_url),
//     style: parseObject(data.style),
//     system: {
//       createdDate: data.create_date || null,
//       createdBy: data.create_by || null,
//       updatedData: data.update_date || null,
//       updatedBy: data.update_by || null,
//       version: data.version || null
//     },
//     displayName: {
//       en: data.text || data.name || data.iconName || data.type || "untitle"
//     },
//     structure: {},
//     cmsProperty: {
//     },
//     property: {
//       items: real
//     }
//   }
// };
