import React, { useState, useEffect } from "react";
import HeaderSection from "./HeaderSection";
import LeftMenuSection from "./LeftMenuSection";
import PropertyWindowSection from "./PropertyWindowSection";
import MainSection from "./MainSection";
import {
  // parsePageList,
  parseProjectList,
  parsePropertyStructureList,
  parseComponentMasterList,
  parseComponentSetList
} from "./parser";
import {
  MuiThemeProvider,
  makeStyles,
  createMuiTheme
} from "@material-ui/core/styles";
import "./theme/default.css";
import "./App.css";
import {
  getListOfComponent,
  getAddPageList,
  // getListOfPage,
  getListOfProject,
  getComponentSetList,
  getOrganizationAllProjectList,
  getOrganizationAllPageList,
  getOrganizationAllComponentSetList
} from "./api";
import { routeType, LeftMenuElement } from "./type/enum";
import { RouteContext } from "./contextComponent";
import classnames from "classnames";
import {
  TreeDataStructure,
  Feb27ProjectInterface,
  MenuItem,
  ErrorInterface,
  PropertyItemStructure,
  PageInterface,
  ComponentMasterStructure,
  ComponentSetStructure
} from "./type/interfaceCommon";

const App: React.FC = () => {
  const MuiTheme = createMuiTheme({
    typography: {
      fontFamily: "var(--Afont-family)"
    },
    palette: {
      primary: {
        main: "#1D7DED"
      }
    },
    overrides: {
      MuiButton: {
        root: {
          background: "var(--Abtn-primary2)",
          color: "var(--Aprimary-color)",
          textTransform: "none"
        }
      },
      MuiPaper: {
        root: {
          color: "var(--Ablack)"
        }
      }
    }
  });

  const useStyles = makeStyles({
    appContainer: {
      color: "var(--Ablack)",
      height: "100%",
      fontSize: "var(--Afont-general)",
      width: "100%",
      position: "fixed",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      overflow: "hidden",
      font: "var(--Afont-M) Lato",
      "& .MuiOutlinedInput-notchedOutline": {
        borderColor: "var(--Aprimary-color-light2)"
      }
    },
    indexContentArea: {
      display: "grid",
      gridTemplateColumns: "310px 1fr",
      width: "100%",
      marginTop: "85px",
      height: "calc(100% - 85px)"
    },
    addHomeContentArea: {
      display: "grid",
      gridTemplateColumns: "300px 1fr 230px",
      width: "100%",
      marginTop: "85px",
      height: "calc(100% - 85px)"
    }
  });
  const classes = useStyles();

  const { IProject, IPage, IComponent, ISetting, IOrgan } = LeftMenuElement;
  const { Index, AddHome, AddProject, Setting, AddComponentSet } = routeType;
  const [routeState, setRouteState] = useState<routeType>(AddProject);
  const [isAskSave, setIsAskSave] = useState<boolean>(false);

  const [selectedTreeNode, setSelectedTreeNode] = useState<
    TreeDataStructure | {}
  >({});
  const [addedItemList, setAddedItemList] = useState<
    ComponentMasterStructure[] | []
  >([]);
  const [pageTemplateList, setPageTemplateList] = useState<
    TreeDataStructure[] | []
  >([]);
  const [pageList, setPageList] = useState<PageInterface[] | []>([]);
  const [propertyList, setPropertyList] = useState<
    PropertyItemStructure[] | []
  >([]);
  const [projectList, setProjectList] = useState<Feb27ProjectInterface[] | []>(
    []
  );

  const [treeDataState, setTreeDataState] = useState<TreeDataStructure[] | []>(
    []
  );
  const [errorObj, setErrorObj] = useState<ErrorInterface>({
    error: false,
    errorMsg: "Something wrong"
  });

  // define state for organization level
  // organization level - page list
  // organization level - componentSet list
  // organization level - project list
  const [organizationID, setOrganizationId] = useState<string>(
    "5e54d84a9106f1fe73a92e8a"
  );
  const [organizationProjectList, setOrganizationProjectList] = useState<
    Array<any>
  >([]);
  const [organizationPageList, setOrganizationPageList] = useState<Array<any>>(
    []
  );
  const [
    organizationComponentSetList,
    setOrganizationComponentSetList
  ] = useState<any>([]);

  const leftMenuConst = {
    Organ: {
      id: IOrgan,
      displayName: "EAB System"
      // fetchDataFunc: (): void => {},
      // updateDataFunc: (): void => {}
    },
    Project: {
      id: IProject,
      displayName: "Project",
      fetchDataFunc: getOrganizationAllProjectList,
      updateDataFUnc: setOrganizationProjectList
    },
    Page: {
      id: IPage,
      displayName: "Page",
      fetchDataFunc: getOrganizationAllPageList,
      updateDataFUnc: organizationPageList
    },
    Component: {
      id: IComponent,
      displayName: "Component Set",
      fetchDataFunc: getOrganizationAllComponentSetList,
      updateDataFUnc: organizationComponentSetList
    },
    Setting: { id: ISetting, displayName: "Project Setting" }
  };

  const [selectedItem, setSelectedItem] = useState<MenuItem>(
    leftMenuConst.Page
  );
  const [currentPageId, setCurrentPageId] = useState<string>("");
  const [currentProjectId, setCurrentProjectId] = useState<string>("");
  const [currentCompId, setCurrentCompId] = useState<string>("");
  const [componentSetList, setComponentSetList] = useState<
    ComponentSetStructure[] | []
  >([]);
  const [pjComptSetList, setPjComptSetList] = useState<Array<any>>([]);
  console.log("[App.tsx] currentProjectId", currentProjectId);
  const openErrorDialogWithMsg = (err = "Something Wrong" as string): void => {
    setErrorObj({
      error: true,
      errorMsg: err
    });
  };

  const closeErrorDialog = (): void => {
    setErrorObj({
      error: false,
      errorMsg: "Something Wrong"
    });
  };

  useEffect(() => {
    console.warn("[App.tsx] useEffect:: []");
    let parsedPropertyList: PropertyItemStructure[] | [] = [];
    getListOfComponent()
      .then(data => {
        setAddedItemList(parseComponentMasterList(data));
        parsedPropertyList = parsePropertyStructureList(data);
        return getAddPageList();
      })
      .then(data => {
        setPageTemplateList(data);
        parsedPropertyList = [...parsedPropertyList, ...data];
        setPropertyList(parsedPropertyList);
      })
      .catch(err => {
        console.error("Error!!", err);
      });
    getAddPageList().then(data => {
      setPageTemplateList(data);
    });
    // const aaa = ["a"];
    // const test = parsePageList(aaa);
    // console.warn("test IT!", test);
    getListOfProject().then(data => {
      const projectList = parseProjectList(data);
      setProjectList(projectList);
    });
    // getListOfPage().then(data => {
    //   const pageList = parsePageList(data);
    //   console.log(pageList);
    //   setPageList(pageList);
    // });
  }, []);

  useEffect(() => {
    getComponentSetList(currentProjectId).then(data => {
      const componentSetList = parseComponentSetList(data);
      setPjComptSetList(componentSetList);
    });
  }, [currentProjectId]);

  useEffect(() => {
    console.warn("[App.tsx] useEffect:: routeState");
    if (routeState !== AddHome) {
      setSelectedTreeNode(""); // reset the currentPage if back to index;
    }
    if (routeState !== AddHome && currentPageId) {
      setCurrentPageId(""); // reset the currentPage if back to index;
    }
    if (routeState !== AddHome && currentCompId) {
      setCurrentCompId(""); // reset the current component if back to index;
    }
    // if (routeState === AddProject && currentProjectId) {
    //   setCurrentProjectId("");
    //   console.log("[App.tsx] routeState", routeState);
    //   getListOfPage()
    //     .then(data => {
    //       const pageList = parsePageList(data);
    //       setPageList(pageList);
    //     })
    //     .catch(err => {
    //       openErrorDialogWithMsg(err.response.data.message);
    //     });
    // }
    switch (routeState) {
      case Index:
        setSelectedItem(leftMenuConst.Page);
        break;
      case AddProject:
        setSelectedItem(leftMenuConst.Project);
        break;
      case Setting:
        setSelectedItem(leftMenuConst.Setting);
        break;
      case AddComponentSet:
        setSelectedItem(leftMenuConst.Component);
      default:
        break;
    }
  }, [routeState]);

  const leftMenuSelection = (item: any): void => {
    if (item === selectedItem) {
      return;
    }
    setSelectedItem(item);
    switch (item.id) {
      case IPage:
        setRouteState(Index);
        break;
      case IProject:
        setRouteState(AddProject);
        break;
      case IComponent:
        setRouteState(AddComponentSet);
        break;
      case ISetting:
        setRouteState(Setting);
        break;
      default:
        break;
    }
  };

  const getLeftMenuArray = (): MenuItem[] => {
    switch (routeState) {
      case AddProject:
        return [
          leftMenuConst.Organ,
          leftMenuConst.Project,
          leftMenuConst.Page,
          leftMenuConst.Component
        ];
      case Index:
      case Setting:
      case AddComponentSet:
        if (currentProjectId) {
          return [
            leftMenuConst.Page,
            leftMenuConst.Component,
            leftMenuConst.Setting
          ];
        } else {
          return [
            leftMenuConst.Organ,
            leftMenuConst.Project,
            leftMenuConst.Page,
            leftMenuConst.Component
          ];
        }
      default:
        return [];
    }
  };

  return (
    <div className={"Main"}>
      <RouteContext.Provider
        value={{
          state: {
            isAskSave,
            organizationID,
            organizationProjectList,
            organizationPageList,
            organizationComponentSetList,
            routeState,
            selectedTreeNode,
            addedItemList,
            treeDataState,
            pageList,
            projectList,
            currentPageId, // Set after select the row
            currentProjectId,
            pageTemplateList,
            currentCompId,
            errorObj,
            propertyList,
            componentSetList,
            pjComptSetList
          },
          func: {
            setIsAskSave,
            setOrganizationId,
            setOrganizationProjectList,
            setOrganizationPageList,
            setOrganizationComponentSetList,
            setRouteState,
            setSelectedTreeNode,
            setAddedItemList,
            setTreeDataState,
            setPageList,
            setProjectList,
            setCurrentPageId,
            setCurrentProjectId,
            setPageTemplateList,
            setCurrentCompId,
            openErrorDialogWithMsg,
            closeErrorDialog,
            setPropertyList,
            setComponentSetList,
            setPjComptSetList
          }
        }}
      >
        <MuiThemeProvider theme={MuiTheme}>
          <header className={classes.appContainer}>
            <HeaderSection headerTitle={"Barista Bar"} />
            <div
              className={classnames({
                [classes.indexContentArea]: routeState !== routeType.AddHome,
                [classes.addHomeContentArea]: routeState === routeType.AddHome
              })}
            >
              <LeftMenuSection
                arrayOfItems={getLeftMenuArray()}
                selectedElement={selectedItem}
                onSelectHandler={leftMenuSelection}
              />

              <MainSection />
              {routeState === AddHome ? console.log("AddHome", AddHome) : null}
              {routeState === AddHome && <PropertyWindowSection />}
            </div>
          </header>
        </MuiThemeProvider>
      </RouteContext.Provider>
    </div>
  );
};

export default App;
