import React, {
  FunctionComponent,
  useState,
  useContext,
  useEffect
} from "react";
import classnames from "classnames";
import { RouteContext } from "./contextComponent";
import {
  TextField,
  Radio,
  Button,
  RadioGroup,
  FormControlLabel
} from "@material-ui/core";
import { parseComponentMasterList } from "./parser";
import { makeStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { TreeDataStructure } from "./type/interfaceCommon";
import { getListOfComponent } from "./api";

interface DialogAddItemPopUpProps {
  addItemToTree: (selectedKeys: string, itemType: string) => void;
  keyOfAddingItem: string;
  dialogTopLeftTitle: string;
  dialogOpen: boolean;
  closeDialog: () => void;
}

const DialogAddItemPopUp: FunctionComponent<DialogAddItemPopUpProps> = (
  props: DialogAddItemPopUpProps
) => {
  const [isAddBtnDisabled, setIsAddBtnDisabled] = useState<boolean>(true);
  const [searchText, setSearchText] = useState<string>("");
  const [selectedAddingItem, setSelectedAddingItem] = useState<string>("");
  const { state, func } = useContext(RouteContext);
  const { addedItemList = [] } = state;
  const { setAddedItemList } = func;
  const [filteredItemList, setFilteredItemList] = useState<TreeDataStructure[]>(
    []
  );
  const {
    addItemToTree,
    closeDialog,
    dialogTopLeftTitle,
    dialogOpen,
    keyOfAddingItem
  } = props;

  useEffect(() => {
    if (dialogOpen === false) {
      setSelectedAddingItem("");
      console.warn("reset the value");
    }
  }, [dialogOpen]);

  useEffect(() => {
    console.log("[DialogAddItemPopUp.tsx] addedItemList", addedItemList);
    getListOfComponent().then(data => {
      setAddedItemList(parseComponentMasterList(data));
    });
    setFilteredItemList(addedItemList);
  }, []);

  // useEffect(() => {
  //   if (selectedAddingItem !== "" && isAddBtnDisabled === true) {
  //     setIsAddBtnDisabled(false);
  //   }
  // })

  const useStyles = makeStyles({
    leftSide: {
      width: "150px"
    },
    searchBar: {
      width: "600px"
    },
    dialogContent: {
      height: "400px",
      padding: "0"
    },
    mainContainer: {
      padding: "var(--Apadding-36)"
    },
    title: {
      fontWeight: 700,
      fontSize: "var(--Afont-M)"
    },
    menuItem: {
      height: "42px",
      fontSize: "var(--Afont-S)",
      lineHeight: "42px",
      textAlign: "left",
      padding: "0 25px"
    },
    selectedItem: {
      color: "var(--Aprimary-color)",
      borderLeft: "var(--Aborder-L) var(--Aprimary-color-light1) solid",
      paddingLeft: "19px"
    },
    normalBorder: {
      border: "var(--Aborder-S) var(--Aline-color) solid",
      // maxWidth: "150px",
      width: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)",
      overflow: "hidden",
      backgroundColor: "#F5F5F5",
      "&:hover": {
        backgroundColor: "#EAF4FF"
      }
    },
    selectedIcon: {
      border: "var(--Aborder-S) var(--Aprimary-color) solid",
      width: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)",
      overflow: "hidden",
      backgroundColor: "#EAF4FF"
    },
    radioGroupFlex: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "flex-start",
      alignItems: "stretch"
    },
    componentTypeStyle: {
      textAlign: "center",
      fontSize: "var(--Afont-XS)",
      paddingTop: "var(--Apadding-XS)",
      paddingBottom: "var(--Apadding-XXXL)"
    }
  });

  const useStylesTypo = makeStyles(
    {
      root: {
        display: "flex"
      }
    },
    { name: "MuiTypography" }
  );
  const useStylesRadio = makeStyles({
    colorPrimary: {
      "&.Mui-checked:hover": {
        backgroundColor: "transparent"
      },
      "&:hover": {
        backgroundColor: "transparent"
      },
      // position: "absolute",
      top: "50%",
      transform: "translate(0%, -50%)"
    }
  });
  const classes = useStyles(props);
  const classesTypo = useStylesTypo(props);
  const classesRadio = useStylesRadio(props);
  const addItemHandler = (): void => {
    addItemToTree(keyOfAddingItem, selectedAddingItem);
    setIsAddBtnDisabled(true);
    closeDialog();
  };

  const cancalHandler = (): void => {
    setIsAddBtnDisabled(true);
    closeDialog();
  };

  const filterList = (searchText: string): void => {
    if (searchText && searchText.length > 0) {
      const updatedList = addedItemList.filter((item: TreeDataStructure) => {
        return (
          item.cmsComponentType
            .toLowerCase()
            .search(searchText.toLowerCase()) !== -1
        );
      });
      setFilteredItemList(updatedList);
    } else {
      setFilteredItemList(addedItemList);
    }
  };

  const radioOnChnage = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setSelectedAddingItem(event.target.value);
    setIsAddBtnDisabled(false);
  };
  return (
    <Dialog
      //   className={classes.dialogGrid}
      open={dialogOpen}
      onClose={closeDialog}
      fullWidth
      maxWidth={"lg"}
    >
      <DialogTitle className={classesTypo.root} id="simple-dialog-title">
        <div className={classes.leftSide}>{dialogTopLeftTitle}</div>
        <TextField
          className={classes.searchBar}
          fullWidth
          margin="none"
          id="outlined-basic"
          variant="outlined"
          value={searchText}
          onChange={(e): void => {
            setSearchText(e.target.value);
            filterList(e.target.value);
          }}
          placeholder={"Search"} // TODO: need to modify
        />
      </DialogTitle>
      <DialogContent
        className={classnames(classesTypo.root, classes.dialogContent)}
        dividers
      >
        <div id={"leftMenuContainer"} className={classes.leftSide}>
          <div className={classnames(classes.menuItem, classes.title)}>
            Component
          </div>
          <div className={classnames(classes.menuItem, classes.selectedItem)}>
            Input
          </div>
        </div>
        <div className={classes.mainContainer}>
          <RadioGroup
            className={classes.radioGroupFlex}
            aria-label="position"
            name="position"
            value={selectedAddingItem}
            onChange={radioOnChnage}
            row
          >
            {filteredItemList.map(
              (item, index): JSX.Element => {
                console.log("[DialogAddItemPopUp.tsx] item", item);
                const { cmsComponentType, displayName } = item;
                const imageUrl =
                  item.imageUrl ||
                  "https://www.bigstockphoto.com/images/homepage/module-6.jpg";
                // const { en } = displayName;
                return (
                  <div key={index}>
                    <FormControlLabel
                      // checked={cmsComponentType === selectedAddingItem}
                      className={
                        selectedAddingItem === cmsComponentType
                          ? classes.selectedIcon
                          : classes.normalBorder
                      }
                      key={index}
                      value={cmsComponentType}
                      labelPlacement={"bottom"}
                      control={
                        <Radio
                          className={classesRadio.colorPrimary}
                          checkedIcon={
                            <img alt={cmsComponentType} src={imageUrl} />
                          }
                          icon={<img alt={cmsComponentType} src={imageUrl} />}
                          color="primary"
                          disableRipple
                        />
                      }
                      label={null}
                      // label={cmsComponentType}
                      // name="addingItem"
                    />
                    <p className={classes.componentTypeStyle}>
                      {displayName.en}
                    </p>
                  </div>
                );
              }
            )}
          </RadioGroup>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={cancalHandler} color="primary">
          Cancel
        </Button>
        <Button
          onClick={addItemHandler}
          color="primary"
          disabled={isAddBtnDisabled}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogAddItemPopUp;
