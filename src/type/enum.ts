export enum routeType {
  Index = "ADD_PAGE", //AddPage
  AddHome = "WORKSPACE", //WorkSpace
  AddProject = "ADD_PROJECT",
  AddComponentSet = "ADD_COMPONENT_SET",
  Setting = "SETTING"
}

export enum levelType {
  OrganizationLevel = "ORGANIZATION_LEVEL",
  ProjectLevel = "PROJECT_LEVEL"
}

export enum itemCategory { // Please expand it if needed
  // Layout = "LAYOUT",
  Inputs = "INPUTS",
  CafeForm = "CAFE",
  CompSet = "COMPONENT_SET"
}

export enum iconSize {
  small = "small",
  large = "large"
}

export enum iconCategory {
  addIcon = "AddIcon",
  deleteIcon = "DeleteIcon"
}

export enum LeftMenuElement {
  IOrgan, // Cannot select
  IProject,
  IPage,
  IComponent,
  ISetting
}
