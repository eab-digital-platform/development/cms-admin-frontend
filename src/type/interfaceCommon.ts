import { LeftMenuElement } from "./enum";

export interface TreeDataStructure {
  type: string; //define component
  // items?: TreeDataStructure[];
  property?: any;
  pageOrComponent?: string;
  cssClass?: string;
  style?: any;
  system?: any;
  cmsProperty?: any;
  displayName?: any;
  keys: string; //unique
  imageUrl?: string;
  [text: string]: any;
}

export interface Feb24TreeStructureDefinedByTeam {
  _id?: string;
  cmsComponentType: string;
  key?: string;
  style?: any;
  muiProps?: any;
  cmsProps?: any;
  items?: any[];
  displayName?: MultipleLangInterface;
}

export interface AddPageDataInterface extends Feb24TreeStructureDefinedByTeam {
  displayName: MultipleLangInterface;
  imageUrl: string;
}

export interface PropertyItemStructure {
  _id?: string; // Dunt know if it is needed
  cmsComponentType: string;
  style: PropertyWindowStructure[];
  muiProps: PropertyWindowStructure[];
  cmsProps: PropertyWindowStructure[];
  imageUrl?: string;
}

export interface ComponentMasterStructure {
  _id?: string;
  cmsComponentType: string;
  style: any;
  muiProps: any;
  cmsProps: any;
  items?: any[];
  displayName: MultipleLangInterface;
  imageUrl?: string;
}

export interface PropertyWindowStructure {
  key: string;
  value: any;
}

export interface MultipleLangInterface {
  en: string;
  [text: string]: string;
}

// export interface PropertyInterface {
//   items?: TreeDataStructure[] | PageInterface[];
//   itemsContent?: string[];
// }

export interface SystemInterface {
  userComponent?: boolean;
  group?: string;
  createdDate: string;
  createdBy: string;
  updatedDate: string;
  updatedBy: string;
  version?: string | number;
}

export interface CmsProperty {
  cssClass: any;
  style: any[];
  structure?: any[];
}

export interface PageInterface {
  type: string;
  system: SystemInterface;
  displayName?: MultipleLangInterface; // TODO: need to ask Kevin to add back
  code: string;
  uId: string;
  level: string;
  description: MultipleLangInterface;
  keys?: string;
}

export interface SourceHost {
  name: string;
  value: string;
}

export interface ProjectSettingInterface {
  theme: string;
  sourceHosts?: SourceHost[];
}

export interface Feb27ProjectInterface {
  system: SystemInterface;
  title: MultipleLangInterface;
  description: MultipleLangInterface;
  uId: string; //_id will be parsed to uid
  settings: ProjectSettingInterface;
}

//Legacy again on 27/02/2020 due to the changing of project setting
export interface ProjectInterface {
  system: SystemInterface;
  title: MultipleLangInterface;
  description: MultipleLangInterface;
  uId: string;
  theme: string;
}

export interface DialogBasicProps {
  dialogTopLeftTitle?: string;
  dialogOpen: boolean;
  closeDialog: () => void;
  onSubmit?: (data: any) => any;
  namePlaceHolder?: string;
  descPlaceHolder?: string;
  name?: string;
  description?: string;
  pageInfo?: any;
}

export interface PresetDetail {
  code: string;
  description: string; //May need to modify if en exist
}

export interface MenuItem {
  displayName?: string; // If empty display name, will use the id as display
  id: LeftMenuElement;
  fetchDataFunc?: Function;
  updateDataFunc?: Function;
}

export interface ErrorInterface {
  error: boolean;
  errorMsg: string;
}

export interface ComponentSetStructure {
  _id: string;
  title?: string;
  description: any;
  cmsComponentType: string;
  style: any;
  cmsProps: any;
  muiProps: any;
  items: any;
}
