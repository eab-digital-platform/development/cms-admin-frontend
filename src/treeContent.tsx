import "rc-tree/assets/index.css";
import "./tree.css";
import { iconSize, iconCategory } from "./type/enum";
import React, {
  FunctionComponent,
  useState,
  useContext,
  useEffect,
  Fragment
} from "react";
import classnames from "classnames";
import Tree, { TreeNode } from "rc-tree";
import { v4 as uuidv4 } from "uuid";
import { TreeDataStructure } from "./type/interfaceCommon";
import { convertTheJsonToAdd } from "./parser";
import DialogAddItemPopUp from "./DialogAddItemPopUp";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import { RouteContext } from "./contextComponent";
import pageIcon from "./assets/pageIcon.png";
import ConfirmDialog from "./common/ConfirmDialog";

// import { Button } from "@material-ui/core";

interface TreeWrapperProps {
  treeData: TreeDataStructure[];
  setTreeData: (item: TreeDataStructure[]) => void;
  // addItemToTree: (selectedKeys: string, itemType: string) => void;
}

const TreeWrapper: FunctionComponent<TreeWrapperProps> = (
  props: TreeWrapperProps
) => {
  const { treeData, setTreeData } = props;
  const { small } = iconSize;
  const { state, func } = useContext(RouteContext);
  const { setIsAskSave } = func;
  const { setSelectedTreeNode } = func;
  const { addIcon, deleteIcon } = iconCategory;
  const [keyAddingItem, setKeyAddingItem] = useState<string>("");
  // const [selectedKey, setSelectedKey] = useState<string[] | []>([]);
  // const [setExpandedKey] = useState<any>([]);
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const [hoverKey, setHoverKey] = useState<string | null>(null);
  const [confirmDeleteDialog, setConfirmDeleteDialog] = useState<boolean>(
    false
  );

  const useStyles = makeStyles({
    draggableContainer: {
      position: "relative",
      color: "var(--Agrey-font-color)"
    },
    iconWrapper: {
      display: "flex",
      justifyContent: "flex-end"
    },
    displayNone: {
      display: "none"
    },
    icon: {
      color: "var(--Agrey-font-color)"
    },
    titleIcon: {
      display: "flex",
      alignItems: "center"
    },
    iconStyle: {
      width: "24px",
      height: "24px"
    },
    treeContainer: {
      padding: "0 0 0 var(--Apadding-24)"
    }
  });
  const treeNode = { padding: "var(--Apadding-14) 0 0 0" };
  const classes = useStyles(props);

  useEffect(() => {
    setSelectedTreeNode(treeData[0]);
  }, []);

  // draggableContainer

  const openDialog = (): void => {
    setDialogOpen(true);
  };

  const closeDialog = (): void => {
    setKeyAddingItem("");
    setHoverKey(null);
    setDialogOpen(false);
  };

  const loop = (data, key, callback): void => {
    data.forEach((item, index, arr) => {
      if (item.key === key) {
        callback(item, index, arr); // TODO: Need to debug
        return;
      }
      if (item.items) {
        loop(item.items, key, callback);
      }
    });
  };

  const deleteItem = (selectedKey: string): void => {
    if (selectedKey && selectedKey.length > 0) {
      // const selectedObj;
      const copyOfTree = [...treeData];
      // const arrayOfItem = btnObj];
      loop(copyOfTree, selectedKey, (item: TreeDataStructure, index, arr) => {
        arr.splice(index, 1);
        // selectedObj = item;
      });
      setTreeData([...copyOfTree]);
      setIsAskSave(true);
    }
  };

  const onSelected = (selectedKey: string[]): void => {
    console.warn("NULLLLLLLLLLLLLLLLLLLLLLl", selectedKey);
    if (selectedKey.length > 0) {
      loop(treeData, selectedKey[0], (item: TreeDataStructure) => {
        func.setSelectedTreeNode(item);
        console.warn("not NULLLLLLLLL", item);
      });
    } else {
      console.warn("I am nulllllllllllll");
    }
  };

  const onAddButtonClick = (
    selectedKey: string,
    itemTypeToAdd: string
  ): void => {
    if (selectedKey && selectedKey.length > 0 && itemTypeToAdd) {
      const { addedItemList } = state;
      let JSONOfItem;
      addedItemList.forEach(itemJson => {
        if (itemTypeToAdd === itemJson.cmsComponentType) {
          // make sure adding the new component "items" property is empty array []
          JSONOfItem = convertTheJsonToAdd({ ...itemJson, items: [] });
        }
      });
      // const selectedObj;
      const copyOfTree = [...treeData];
      // const arrayOfItem = btnObj];
      if (JSONOfItem) {
        loop(copyOfTree, selectedKey, (item: any) => {
          item.items = item.items || [];
          item.items.push(JSONOfItem);
        });
      }

      setTreeData([...copyOfTree]);
      setIsAskSave(true);
    }
  };

  const onConfirm = (): void => {
    if (hoverKey) {
      deleteItem(hoverKey);
    }
    setConfirmDeleteDialog(false);
  };

  const treeNodeContent = (
    currentKey: string,
    itemArray: string[]
  ): JSX.Element => {
    const addItemIcon = (index: number): JSX.Element => {
      return (
        <AddIcon
          key={`add_${index}`}
          onClick={(event): void => {
            event.stopPropagation();
            openDialog();
            setKeyAddingItem(currentKey);
          }}
          fontSize={small}
          className={classes.icon}
        />
      );
    };

    const deleteItemIcon = (index: number): JSX.Element => {
      return (
        <DeleteIcon
          key={`delete_${index}`}
          onClick={(event): void => {
            event.stopPropagation();
            setConfirmDeleteDialog(true);
          }}
          fontSize={small}
          className={classes.icon}
        />
      );
    };

    const chooseIconSet = (itemArray: string[]): (JSX.Element | null)[] => {
      return itemArray.map((item, index) => {
        switch (item) {
          case addIcon:
            return addItemIcon(index);
          case deleteIcon:
            return deleteItemIcon(index);
          default:
            return null;
        }
      });
    };

    return (
      <div
        key={Math.random()}
        className={classnames({
          [classes.iconWrapper]: hoverKey === currentKey,
          [classes.displayNone]: hoverKey !== currentKey
        })}
      >
        {chooseIconSet(itemArray)}
      </div>
    );
  };

  const loopItem = (data): JSX.Element => {
    return data.map(item => {
      const {
        key,
        // pageOrComponent = "component",
        items,
        cmsComponentType,
        displayName = { en: "" }
      } = item;
      const { en } = displayName;
      // const { items } = property;
      const displayKey =
        key ||
        `${cmsComponentType ? cmsComponentType : "untitled"}_${uuidv4()}`;
      if (items) {
        // have child or not have
        if (items.length > 0) {
          //have child
          return (
            <TreeNode
              icon={(): JSX.Element =>
                treeNodeContent(displayKey, [addIcon, deleteIcon])
              }
              className={"testingTreeee"}
              key={displayKey}
              title={
                <div className={classes.titleIcon}>
                  <img
                    alt={cmsComponentType}
                    className={classes.iconStyle}
                    // src={pageOrComponent === "page" ? pageIcon : componentIcon}
                    src={pageIcon}
                  />{" "}
                  {/* {pageOrComponent === "page" ? <PageIcon /> : <ComponentIcon />} */}
                  {en || cmsComponentType || "untitle"}
                </div>
              } // TODO: change the display name to a meaningful item
              style={treeNode}
            >
              {loopItem(items)}
            </TreeNode>
          );
        } else {
          //child can be added but no now
          return (
            <TreeNode
              icon={(): JSX.Element =>
                treeNodeContent(displayKey, [addIcon, deleteIcon])
              }
              style={treeNode}
              key={displayKey}
              title={
                <div className={classes.titleIcon}>
                  {/* {pageOrComponent === "page" ? <PageIcon /> : <ComponentIcon />} */}
                  <img
                    alt={cmsComponentType}
                    className={classes.iconStyle}
                    // src={pageOrComponent === "page" ? pageIcon : componentIcon}
                    src={pageIcon}
                  />{" "}
                  {en || cmsComponentType || "untitle"}
                </div>
              }
            />
          );
        }
      } else {
        //cannot add item
        return (
          <TreeNode
            icon={(): JSX.Element => treeNodeContent(displayKey, [deleteIcon])}
            style={treeNode}
            key={displayKey}
            title={
              <div className={classes.titleIcon}>
                {/* {pageOrComponent === "page" ? <PageIcon /> : <ComponentIcon />} */}
                <img
                  alt={cmsComponentType}
                  className={classes.iconStyle}
                  // src={pageOrComponent === "page" ? pageIcon : componentIcon}
                  src={pageIcon}
                />{" "}
                {en || cmsComponentType || "untitle"}
              </div>
            }
          />
        );
      }
    });
  };

  // const onExpand = (expandedKeys): void => {
  //   console.warn("Expanded", expandedKeys);
  //   setExpandedKey(expandedKeys);
  // };

  const onDrop = (info): void => {
    console.log("drop", info);
    const dropKey = info.node.props.eventKey || "";
    const dragKey = info.dragNode.props.eventKey || "";
    const dropPos = info.node.props.pos.split("-") || "";
    const dropPosition =
      info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const data = [...treeData];

    // Find dragObject
    let dragObj;
    loop(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, item => {
        // item.property = item.property || {};
        item.items = item.items || [];
        item.items.push(dragObj);
      });
    } else if (
      (info.node.props.items || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      loop(data, dropKey, item => {
        // item.property = item.property || {};
        item.items = item.items || [];
        item.items.unshift(dragObj);
      });
    } else {
      // Drop on the gap
      let ar;
      let i;
      loop(data, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }

    setTreeData(data);
  };
  return (
    <Fragment>
      <div className={classes.draggableContainer}>
        <Tree
          className={classes.treeContainer}
          defaultExpandParent={true}
          defaultExpandAll={true}
          // expandedKeys={expandedKey}
          // onExpand={onExpand}
          selectedKeys={[state.selectedTreeNode.key]}
          onMouseEnter={({ node }): void => {
            setHoverKey(node.props.eventKey);
          }}
          onMouseLeave={(): void => {
            setHoverKey(null);
          }}
          // autoExpandParent={this.state.autoExpandParent}
          draggable
          // onDragStart={this.onDragStart}
          // onDragEnter={this.onDragEnter}
          onDrop={onDrop}
          onSelect={onSelected}
        >
          {loopItem(treeData)}
        </Tree>
      </div>
      <DialogAddItemPopUp
        keyOfAddingItem={keyAddingItem}
        dialogTopLeftTitle={"Add Item"}
        dialogOpen={dialogOpen}
        closeDialog={closeDialog}
        addItemToTree={onAddButtonClick}
      />
      <ConfirmDialog
        open={confirmDeleteDialog}
        onConfirm={onConfirm}
        onClose={(): void => setConfirmDeleteDialog(false)}
      />
    </Fragment>
  );
};

export default TreeWrapper;
