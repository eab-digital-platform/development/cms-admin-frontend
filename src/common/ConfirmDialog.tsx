import React, { FunctionComponent } from "react";
import classnames from "classnames";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";

export interface SimpleDialogProps {
  title?: string;
  content?: string | JSX.Element;
  open: boolean;
  errorDialog?: boolean; //use for showing only one button
  onConfirm: () => any;
  onClose: () => any;
}

const ConfirmDialog: FunctionComponent<SimpleDialogProps> = (
  props: SimpleDialogProps
) => {
  const { open, onConfirm, onClose, errorDialog } = props;
  const title = props.title || "Delete";
  const content = props.content || "Are you sure you want to delete this item?";
  const useStyles = makeStyles({
    main: {
      width: "100%"
    },
    dialogContainer: {
      "& .MuiPaper-root.MuiDialog-paper": {
        width: "20%"
      }
    },
    title: {
      fontWeight: 700,
      fontSize: "var(--Afont-M)"
    },
    normalBorder: {
      border: "var(--Aborder-S) var(--Aline-color) solid",
      maxWidth: "150px",
      height: "135px",
      fontSize: "var(--Afont-XS)",
      color: "var(--Agrey-font-color)",
      margin: "var(--Apadding-XXXXS) var(--Apadding-XXS)",
      padding: "var(--Apadding-XXXXS)"
    },
    pageCode: {
      height: "46px",
      marginBottom: "var(--Apadding-XXXS)"
    },
    titleMain: {
      alignItems: "center",
      fontSize: "var(--Afont-XM)"
    },
    buttonWrapper: {
      padding: "var(--Apadding-XS) var(--Apadding-L) var(--Apadding-L)"
    }
  });
  const classes = useStyles(props);
  return (
    <Dialog
      className={classes.dialogContainer}
      open={open}
      onClose={onClose}
      maxWidth={"xs"}
    >
      <DialogTitle id="simple-dialog-title">
        <div className={classnames(classes.titleMain)}>
          <p>{title}</p>
        </div>
      </DialogTitle>
      <DialogContent>
        <div className={classes.main}>{content}</div>
      </DialogContent>
      <DialogActions className={classes.buttonWrapper}>
        <Button onClick={onClose}>{errorDialog ? "Close" : "Cancel"}</Button>
        {!errorDialog && (
          <Button onClick={onConfirm} variant="contained" color="primary">
            Confirm
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
