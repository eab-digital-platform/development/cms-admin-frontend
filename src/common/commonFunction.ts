import {
  ProjectSettingInterface
} from "../type/interfaceCommon";

export const findCodeById = (list: any[], id: string): string => {
  const filterdElement: any[] = list.filter(item => item.uId === id);
  if (!filterdElement || filterdElement.length === 0) {
    console.warn(`Id cannot be found on List`);
  } else if (filterdElement.length > 1) {
    console.warn(`There are more of one element within this id`);
  } else {
    return filterdElement[0].code;
  }
  return "";
};

// Please noted only en is return now
export const findDescriptionById = (list: any[], id: string): string => {
  const filterdElement: any[] = list.filter(item => item.uId === id);
  if (!filterdElement || filterdElement.length === 0) {
    console.warn(`Id cannot be found on List`);
  } else if (filterdElement.length > 1) {
    console.warn(`There are more of one element within this id`);
  } else {
    return filterdElement[0].description.en;
  }
  return "";
};

export const findSettingsByPjId = (
  list: any[],
  id: string
): ProjectSettingInterface => {
  const filterdElement: any[] = list.filter(item => item.uId === id);
  if (!filterdElement || filterdElement.length === 0) {
    console.warn(`Id cannot be found on List`);
  } else if (filterdElement.length > 1) {
    console.warn(`There are more of one element within this id`);
  } else {
    return filterdElement[0].settings;
  }
  return { theme: "", sourceHosts: [] };
};

export const findTitleById = (list: any[], id: string): string => {
  const filterdElement: any[] = list.filter(item => item.uId === id);
  if (!filterdElement || filterdElement.length === 0) {
    console.warn(`Id cannot be found on List`);
  } else if (filterdElement.length > 1) {
    console.warn(`There are more of one element within this id`);
  } else {
    return filterdElement[0].title.en;
  }
  return "";
};

export const validatePercentage = (str: string): boolean => {
  const regex = /\b(?<!\.)(?:\d|[1-9]\d|100)(?:(?<!100)\.\d+)?%/;
  return regex.test(str);
};
