import React, {
  FunctionComponent,
  useState,
  useEffect,
  useContext,
  Fragment
} from "react";
import { RouteContext } from "../contextComponent";
import classnames from "classnames";
import {
  FormControlLabel,
  Checkbox,
  FormControl,
  FormGroup,
  Button,
  TextField
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  Chip
} from "@material-ui/core";
// import DialogContent from "@material-ui/core/DialogContent";
// import DialogActions from "@material-ui/core/DialogActions";
// import Dialog from "@material-ui/core/Dialog";
import { SimpleDialogProps } from "./ConfirmDialog";
import { movePageToOtherProject, getListOfPage } from "../api";
import { parsePageList } from "../parser";
import { routeType } from "../type/enum";

interface CopyDialogProps extends SimpleDialogProps {
  list?: ListOfContent[];
  selectedPageId: string;
}

interface ListOfContent {
  listTitle?: string;
  listItems: string[];
}

const CopyDialog: FunctionComponent<CopyDialogProps> = (
  props: CopyDialogProps
) => {
  const { state, func } = useContext(RouteContext);
  const { currentProjectId } = state;
  const { setPageList, setRouteState } = func;
  const {
    open,
    onConfirm,
    onClose,
    title,
    content,
    list,
    selectedPageId
  } = props;
  const [searchText, setSearchText] = useState<string>("");
  const [selectedListItem, setSelectedListItem] = useState<any[]>([]);
  const [filteredList, setFilteredList] = useState<
    ListOfContent[] | undefined
  >();
  const [movePageId] = useState<string>(selectedPageId);

  // console.log("[CopyDialog.tsx] list", list);
  // console.log("[CopyDialog.tsx] selectedListItem", selectedListItem);
  // console.log("[CopyDialog.tsx] movePageId", movePageId);

  useEffect(() => {
    setFilteredList(list);
  }, [list]);

  const useStyles = makeStyles({
    titleMain: {
      width: "100%",
      display: "grid",
      gridTemplateColumns: "1fr 4fr 1fr",
      alignItems: "center",
      padding: "0 var(--Apadding-XXS)",
      fontSize: "var(--Afont-XM)"
    },
    searchBar: {
      height: "37px",
      padding: "0px var(--Apadding-XL)",
      display: "grid",
      alignItems: "center",
      gridTemplateColumns: "1fr 77px",
      gridColumnGap: "var(--Apadding-XXXL)",
      "& .MuiOutlinedInput-root": {
        height: "36px"
      }
    },
    dialogMainContainer: {
      height: "60vh",
      display: "grid",
      gridTemplateColumns: "1fr 220px"
    },
    copyDialogLGStyle: {
      "&.MuiDialog-paper.MuiDialog-paperWidthLg": {
        width: "70%"
      },
      "& .MuiDialogContent-root": {
        paddingLeft: "0px"
      }
    },
    listPanel: {
      overflow: "auto",
      fontSize: "13px",
      color: "#707070"
    },
    chosenOne: {
      overflow: "auto",
      padding: "24px 18px",
      fontSize: "14px"
    },
    chosenListContainer: {
      width: "90%",
      "& .MuiChip-root": {
        margin: "var(--Apadding-XXXXXS)"
      }
    },
    itemTitle: {
      padding: "var(--Apadding-M) 58px",
      fontWeight: "bold"
    },
    itemContent: {
      padding: "var(--Apadding-S) var(--Apadding-XL)",
      "& fieldset.MuiFormControl-root": {
        display: "block"
      },
      "& .MuiFormControlLabel-root .MuiFormControlLabel-label": {
        fontSize: "13px",
        padding: "var(--Apadding-XS) var(--Apadding-XL)"
      }
    },
    labelBox: {
      padding: "var(--Apadding-S) var(--Apadding-XL)"
    },
    selectedBox: {
      backgroundColor: "var(--Aprimary-color)",
      color: "var(--Awhite-font-color)",
      "& .MuiCheckbox-colorPrimary.Mui-checked": {
        color: "var(--Awhite-font-color)"
      }
    },
    locationTitle: {
      paddingBottom: "13px",
      color: "#707070"
    }
  });
  const classes = useStyles(props);

  const onSearchChange = (event): void => {
    setSearchText(event.target.value);
  };

  const onCloseDialog = (): void => {
    onClose();
    setSearchText("");
    setSelectedListItem([]);
  };

  const handleDelete = (name: string): void => {
    const numberOfIndex = selectedListItem.indexOf(name);
    if (numberOfIndex > -1) {
      const newSelected = [...selectedListItem];
      newSelected.splice(numberOfIndex, 1);
      setSelectedListItem(newSelected);
    }
  };

  const onSelectChange = name => (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    if (event.target.checked) {
      setSelectedListItem([...selectedListItem, name]);
    } else {
      //   const array = ["aaa", "ab"];
      //   const index = selectedListItem.indexOf(name);
      handleDelete(name);
    }
  };

  const confirmHandler = (): void => {
    for (let i = 0; i < selectedListItem.length; i++) {
      const projectId = selectedListItem[i].uId;
      movePageToOtherProject(movePageId, projectId).then(() => {
        getListOfPage(currentProjectId).then(data => {
          const pageList = parsePageList(data);
          setPageList(pageList);
          setRouteState(routeType.AddProject);
          onConfirm();
        });
      });
    }
  };

  const returnMainContent = (): JSX.Element | string => {
    if (content) {
      return content;
    } else {
      if (filteredList) {
        return (
          <div className={classes.dialogMainContainer}>
            <div className={classes.listPanel}>
              {filteredList.map((item, index) => {
                let updatedList;
                if (searchText && searchText.length > 0) {
                  // updatedList = item.listItems.filter((ele: string) => {
                  //   return (
                  //     ele.toLowerCase().search(searchText.toLowerCase()) !== -1
                  //   );
                  updatedList = item.listItems.filter((ele: any) => {
                    return (
                      ele.title.en
                        .toLowerCase()
                        .search(searchText.toLowerCase()) !== -1
                    );
                  });
                } else {
                  updatedList = item.listItems;
                }
                return (
                  <Fragment key={index}>
                    <div
                      className={classes.itemTitle}
                      key={`${item.listTitle}`}
                    >
                      {item.listTitle}
                    </div>
                    <div className={classes.itemContent}>
                      <FormControl component="fieldset">
                        <FormGroup>
                          {updatedList &&
                            updatedList.map((item, i: number) => {
                              const selected: boolean =
                                selectedListItem.filter(selectedItem => {
                                  // console.log("[CopyDialog.tsx] selectedItem", selectedItem)
                                  return (
                                    selectedItem.title.en === item.title.en
                                  );
                                }).length > 0;
                              return (
                                <FormControlLabel
                                  key={`${item.title.en}_${i}`}
                                  className={classnames(classes.labelBox, {
                                    [classes.selectedBox]: selected
                                  })}
                                  control={
                                    <Checkbox
                                      color="primary"
                                      checked={selected}
                                      onChange={onSelectChange(item)}
                                      value={item.title.en}
                                    />
                                  }
                                  label={item.title.en}
                                />
                              );
                            })}
                        </FormGroup>
                      </FormControl>
                    </div>
                  </Fragment>
                );
              })}
            </div>
            <div className={classes.chosenOne}>
              <p className={classes.locationTitle}>Location chosen: </p>
              <div className={classes.chosenListContainer}>
                {selectedListItem.map((item, index) => {
                  return (
                    <Chip
                      key={`${index}#${item.title.en}`}
                      variant="outlined"
                      color="primary"
                      onDelete={(): void => handleDelete(item.title.en)}
                      label={item.title.en}
                    />
                  );
                })}
              </div>
            </div>
          </div>
        );
      } else {
        return <div></div>;
      }
    }
  };
  return (
    <Dialog
      className={classes.copyDialogLGStyle}
      open={open}
      onClose={onCloseDialog}
      maxWidth={"lg"}
      fullWidth
    >
      <DialogTitle id="simple-dialog-title">
        <div className={classes.titleMain}>
          <p>{title}</p>
          <TextField
            className={classnames(classes.searchBar)}
            fullWidth
            margin="none"
            id="outlined-basic"
            variant="outlined"
            value={searchText}
            onChange={onSearchChange}
            placeholder={"Search Projects..."}
          />
        </div>
      </DialogTitle>
      <DialogContent dividers>
        <div className="dialogMainContainer">{returnMainContent()}</div>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCloseDialog}>Cancel</Button>
        <Button
          disabled={selectedListItem.length <= 0}
          onClick={confirmHandler}
          variant="contained"
          color="primary"
        >
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CopyDialog;
