import axios from "axios";
import environmentVariable from "../config/config.json";

// import config from "../config";

// const apiEndpoint = "http://localhost:3000";
// const apiEndpoint = "http://cmsadminapi-dp.eab.openshift";
const apiEndpoint = environmentVariable.apiEndpoint;
axios.defaults.baseURL = apiEndpoint;

export function setBaseURL(url) {
  axios.defaults.baseURL = url;
}

axios.interceptors.request.use(
  config => {
    config.headers["Content-Type"] = "application/json";
    return config;
  },
  error => Promise.reject(error)
);

// Get
export async function getListOfComponent(): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/componentMaster/list/`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Component List`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getAddPageList(): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/pageTemplate/list`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Page Template`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getListOfPage(pjId?: string): Promise<any> {
  try {
    let res;
    if (pjId) {
      res = await fetch(`${apiEndpoint}/project/${pjId}/page/list/`);
      if (res.status !== 200) {
        throw new Error(`Error occur on getting Page List from project`);
      }
    } else {
      res = await fetch(`${apiEndpoint}/pageMaster/list/`);
      if (res.status !== 200) {
        throw new Error(`Error occur on getting Page List`);
      }
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getPageListOfProject(projectId: string): Promise<any> {
  try {
    const response = await axios.get(
      `${apiEndpoint}/project/${projectId}/page/list`
    );
    return response.data;
  } catch (err) {
    console.error(err);
  }
}

export async function getListOfPageByProject(pjId?: string): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/project/${pjId}/page/list/`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Page List from project`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getInitPageJson(id: string): Promise<any> {
  // Only use for the first add page
  try {
    const res = await fetch(`${apiEndpoint}/pageTemplate/id/${id}/`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Page Template Json`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getPageJson(id: string): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/pageMaster/id/${id}/`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Page Json`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getListOfProject(): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/project/list/`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Project List`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function getProjectSettingById(pgId: string): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/project/${pgId}`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting Project Setting`);
    }
    return res.json();
  } catch (err) {
    console.error("Fetch Error: ", err);
  }
}

export async function getComponentSetList(pgId: string): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/project/${pgId}/componentSet/list`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting component set list`);
    }
    return res.json();
  } catch (err) {
    console.log("Fetch Error: ", err);
  }
}

export async function getOrganizationAllProjectList(
  organizationId: string
): Promise<any> {
  try {
    const res = await fetch(
      `${apiEndpoint}/organization/${organizationId}/project/list`
    );
    if (res.status !== 200) {
      throw new Error(`Error occur on getting organization all project list`);
    }
    return res.json();
  } catch (err) {
    console.log(err);
  }
}

export async function getOrganizationAllPageList(
  organizationId: string
): Promise<any> {
  try {
    const res = await fetch(
      `${apiEndpoint}/organization/${organizationId}/page/list`
    );
    if (res.status !== 200) {
      throw new Error(`Error occur on getting organization all page list`);
    }
    return res.json();
  } catch (err) {
    console.log(err);
  }
}

export async function getOrganizationAllComponentSetList(
  organizationId: string
): Promise<any> {
  try {
    const res = await fetch(
      `${apiEndpoint}/organization/${organizationId}/componentSet/list`
    );
    if (res.status !== 200) {
      throw new Error(
        `Error occur on getting organization all component set list`
      );
    }
    return res.json();
  } catch (err) {
    console.log(err);
  }
}

export async function getOrganizationDetail(
  organizationId: string
): Promise<any> {
  try {
    const res = await fetch(`${apiEndpoint}/organization/${organizationId}`);
    if (res.status !== 200) {
      throw new Error(`Error occur on getting organization detail`);
      return res.json();
    }
  } catch (err) {
    console.log(err);
  }
}

// POST
export async function addComponentSet(data: any): Promise<any> {
  try {
    return axios.post(`${apiEndpoint}/project`, data).then(result => {
      console.warn("Result in api addCompSet");
      return result.data;
    });
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function addPjComponentSet(pjId: any, data: any): Promise<any> {
  try {
    return axios
      .post(`${apiEndpoint}/project/${pjId}/componentSet`, data)
      .then(result => {
        console.warn("Result in api addPjCompSet");
        return result.data;
      });
  } catch (err) {
    console.error("Create Project Component Set Error :", err);
  }
}

export async function addPage(data: any, pjId?: string): Promise<any> {
  try {
    console.warn("Add page api calling projectID: ", pjId);
    if (pjId) {
      return axios
        .post(`${apiEndpoint}/project/${pjId}/pageMaster`, data)
        .then(result => {
          console.warn(`Result in api addPage from project ${pjId}`);
          return result.data;
        });
    } else {
      return axios.post(`${apiEndpoint}/pageMaster`, data).then(result => {
        console.warn("Result in api addPage");
        return result.data;
      });
    }
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function addProject(data: any): Promise<any> {
  try {
    return axios
      .post(`${apiEndpoint}/project`, data)
      .then(result => result.data);
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function cloneProject(pjId: any): Promise<any> {
  try {
    return axios
      .post(`${apiEndpoint}/project/${pjId}/clone`)
      .then(result => result.data);
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function clonePage(pageId: string): Promise<any> {
  try {
    return axios
      .post(`${apiEndpoint}/pageMaster/${pageId}/clone`)
      .then(result => {
        console.log("[Index.tsx] clonePage result", result);
        return result.data;
      });
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function uploadFileToPageMaster(
  pageId: string,
  data: any
): Promise<any> {
  try {
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    return axios
      .post(`${apiEndpoint}/pageMaster/${pageId}/upload`, data, config)
      .then(result => {
        console.log("[index.tsx] api uploadFileToPageMaster pageId:", pageId);
        console.log("[index.tsx] api uploadFileToPageMaster result:", result);
        return result.data;
      });
  } catch (err) {
    console.error("Upload Error :", err);
  }
}

export async function movePageToOtherProject(
  pageId: string,
  projectId: any
): Promise<any> {
  const data = {
    level: "project",
    targetId: projectId
  };
  try {
    axios
      .post(`${apiEndpoint}/pageMaster/${pageId}/move`, data)
      .then(response => {
        console.log(
          "[index.tsx] api movePageToOtherProject response:",
          response
        );
        return response.data;
      });
  } catch (err) {
    console.error("Move Page Error :", err);
  }
}

//PUT
export async function savePage(id: string, data: any): Promise<any> {
  try {
    return axios
      .put(`${apiEndpoint}/pageMaster/id/${id}`, data)
      .then(result => result.data);
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function updatePage(id: string, data: any): Promise<any> {
  try {
    return axios
      .put(`${apiEndpoint}/project/${id}`, data)
      .then(result => result.data);
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function updateProject(id: string, data: any): Promise<any> {
  try {
    return axios
      .put(`${apiEndpoint}/project/${id}`, data)
      .then(result => result.data);
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

export async function updateProjectComptSet(
  pjId: string,
  comptSetId: string,
  data: any
): Promise<any> {
  try {
    return axios
      .put(`${apiEndpoint}/project/${pjId}/componentSet/${comptSetId}`, data)
      .then(result => result.data);
  } catch (err) {
    console.error("Fetch Error :", err);
  }
}

//DEL
export async function deletePage(pageId: string): Promise<any> {
  try {
    return axios
      .delete(`${apiEndpoint}/pageMaster/id/${pageId}`)
      .then(result => result.data)
      .catch(err => err);
  } catch (err) {
    console.error("Fetch Error :", err);
    return err;
  }
}

export async function deleteProject(pjId: string): Promise<any> {
  try {
    return axios
      .delete(`${apiEndpoint}/project/${pjId}`)
      .then(result => result.data)
      .catch(err => err);
  } catch (err) {
    console.error("Fetch Error :", err);
    return err;
  }
}
