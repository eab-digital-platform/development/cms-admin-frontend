import React, {
  FunctionComponent,
  useState,
  useEffect,
  useContext,
  Fragment
} from "react";
// import HeaderSection from "./HeaderSection";
// import LeftMenuSection, { MenuItem } from "./LeftMenuSection";
// import MainSection from "./MainSection";
import { makeStyles } from "@material-ui/core/styles";
import classnames from "classnames";
import _ from "lodash";
import "./theme/default.css";
import "./App.css";
// import { routeType } from "./type/enum";
import { RouteContext } from "./contextComponent";
// import classnames from "classnames";
import {
  TreeDataStructure,
  PropertyWindowStructure
} from "./type/interfaceCommon";
import { TextField } from "@material-ui/core";
import UploadFileBox from "./components/UploadFileBox/UploadFileBox";
import DropDownSelector from "./components/DropDownSelector/DropDownSelector";
import { ReactComponent as ImageSearchIcon } from "./assets/ImageSearchIcon.svg";
import { ReactComponent as TextAreaIcon } from "./assets/TextAreaIcon.svg";
import BooleanSelector from "./components/BooleanSelector/BooleanSelector";
import TextAreaBox from "./components/TextAreaBox/TextAreaBox";
import PageSelector from "./components/PageSelector/PageSelector";
import ColorSelector from "./components/ColorSelector/ColorSelector";
// import BooleanSelector from "./components/BooleanSelector/BooleanSelector";

const PropertyWindowSection: FunctionComponent<any> = () => {
  const { state, func } = useContext(RouteContext);
  // const [styleState, setStyleState] = useState<any>({});
  const [muiPropsState, setMuiPropsState] = useState<
    PropertyWindowStructure | any
  >({});
  const [cmsPropsState, setCmsPropsState] = useState<
    PropertyWindowStructure | any
  >({});
  const [styleState, setStyleState] = useState<PropertyWindowStructure | any>(
    {}
  );
  // const [cssClassState, setCssClassState] = useState<string>("");
  const [displayNameEn, setDisplayNameEn] = useState<string>("");
  const [onMouseIn, setOnMouseIn] = useState<boolean>(false);
  const {
    selectedTreeNode,
    treeDataState,
    propertyList,
    currentProjectId
  } = state;
  const { setTreeDataState, setIsAskSave } = func;
  const { muiProps, cmsProps, style, displayName = {} } = selectedTreeNode;
  const [selectedTypeElement, setSelectedTypeElement] = useState<any>({});
  const { en = "" } = displayName;
  const [isOpenUploadDialog, setIsOpenUploadDialog] = useState<boolean>(false);
  const [isOpenTextArea, setIsOpenTextArea] = useState<boolean>(false);
  useEffect(() => {
    const selectedItemTypeJSON = propertyList.filter(item => {
      return item.cmsComponentType === selectedTreeNode.cmsComponentType;
    })[0];
    console.log("[PropertyWindowSection.tsx] propertyList", propertyList);
    setSelectedTypeElement(selectedItemTypeJSON);
  }, []);

  const useStyles = makeStyles({
    selectedTreeDisplayName: {
      padding:
        "var(--Apadding-XXM) var(--Apadding-M) var(--Apadding-XXS) var(--Apadding-M)",
      backgroundColor: "var(--Aprimary-color-light1)",
      fontSize: "var(--Afont-base)",
      lineHeight: "var(--Afont-M)",
      color: "var(--Awhite-font-color)",
      "& .MuiInputBase-input.MuiInput-input": {
        fontSize: "13px",
        padding: "0",
        paddingBottom: "var(--Apadding-XXXXXS)",
        color: "var(--Awhite-font-color)"
      },
      "& .MuiInput-underline:before, .MuiInput-underline:hover:not(.Mui-disabled):before, .MuiInput-underline:after": {
        borderColor: "var(--Awhite-font-color)"
      }
    },
    styleContainer: {
      padding:
        "var(--Apadding-S) var(--Apadding-M) var(--Apadding-XL) var(--Apadding-M)",
      borderBottom: "var(--Aborder-S) solid var(--Aline-color)"
    },
    lastElement: {
      border: "none"
    },
    sectionTitle: {
      padding:
        "var(--Apadding-XXM) var(--Apadding-M) var(--Apadding-XXS) var(--Apadding-M)",
      fontSize: "var(--Afont-base)",
      lineHeight: "var(--Afont-M)"
    },
    proWindow: {
      overflow: "auto"
    },
    IamText: {
      display: "block",
      cursor: "text",
      textOverflow: "ellipsis",
      overflow: "hidden",
      whiteSpace: "nowrap"
    },
    flexRow: {
      display: "flex",
      justifyContent: "space-between"
    }
  });
  const classes = useStyles();

  const loop = (data, key, callback): void => {
    data.forEach((item, index, arr) => {
      if (item.key === key) {
        callback(item, index, arr);
        return;
      }
      if (item.items) {
        loop(item.items, key, callback);
      }
    });
  };

  useEffect(() => {
    const selectedItemTypeJSON = propertyList.filter(item => {
      // return item.cmsComponentType === "TEXT";
      console.log(
        "[PropertyWindowSection.tsx] item.cmsComponentType",
        item.cmsComponentType
      );
      console.log(
        "[PropertyWindowSection.tsx] selectedTreeNode.cmsComponentType",
        selectedTreeNode.cmsComponentType
      );
      return item.cmsComponentType === selectedTreeNode.cmsComponentType;
    })[0];

    console.log("[PropertyWindowSection.tsx] propertyList", propertyList);
    if (selectedItemTypeJSON) {
      setSelectedTypeElement(selectedItemTypeJSON);
    } else {
      setSelectedTypeElement({ cmsProps: [], muiProps: [], style: [] });
    }
    console.log(
      "[PropertyWindowSection.tsx] selectedTreeNode",
      selectedTreeNode
    );
    console.log(
      "[PropertyWindowSection.tsx] selectedItemTypeJSON",
      selectedItemTypeJSON
    );
    if (!_.isEmpty(selectedTreeNode) && selectedItemTypeJSON) {
      console.warn("Node!!!!!!!!!", selectedTreeNode);
      let tempMui: { [x: string]: any } = {};
      if (!_.isEmpty(muiProps)) {
        // If there are style now
        tempMui = { ...muiProps };
      }
      selectedItemTypeJSON.muiProps.forEach(property => {
        tempMui = {
          ...tempMui,
          [property.key]: tempMui[property.key] ? tempMui[property.key] : ""
        };
      });
      setMuiPropsState(tempMui);

      let tempCms: { [x: string]: any } = {};
      if (!_.isEmpty(cmsProps)) {
        // If there are style now
        tempCms = { ...cmsProps };
      }
      selectedItemTypeJSON.cmsProps.forEach(property => {
        tempCms = {
          ...tempCms,
          [property.key]: tempCms[property.key] ? tempCms[property.key] : ""
        };
      });
      setCmsPropsState(tempCms);

      let tempStyle: { [x: string]: any } = {};
      if (!_.isEmpty(style)) {
        tempStyle = { ...style };
      }
      selectedItemTypeJSON.style.forEach(property => {
        tempStyle = {
          ...tempStyle,
          [property.key]: tempStyle[property.key] ? tempStyle[property.key] : ""
        };
      });
      setStyleState(tempStyle);
      setDisplayNameEn(en || selectedTreeNode.cmsComponentType);
    } else {
      setStyleState({});
      setMuiPropsState({});
      console.log("[PropertyWindowSection.tsx] setCmsPropsState");
      setCmsPropsState({});
    }
  }, [selectedTreeNode]);

  const setSelectedNodeValueToTree = (
    fieldName: string,
    fieldValue: any
  ): void => {
    const copyOfTree = [...treeDataState];
    loop(copyOfTree, selectedTreeNode.key, (item: TreeDataStructure) => {
      item[fieldName] = fieldValue;
    });
    setTreeDataState([...copyOfTree]);
  };

  const triggerSetSelectedNodeValueToTree = (fieldName: string): void => {
    if (fieldName === "style") {
      setSelectedNodeValueToTree(fieldName, styleState);
    } else if (fieldName === "muiProps") {
      setSelectedNodeValueToTree(fieldName, muiPropsState);
    } else if (fieldName === "cmsProps") {
      setSelectedNodeValueToTree(fieldName, cmsPropsState);
    }
  };

  const MuiFieldOnChange = (event, key): void => {
    const tempMuiPropsState = {
      ...muiPropsState,
      [key]: event.currentTarget.value
    };
    if (!_.isEmpty(tempMuiPropsState)) {
      setMuiPropsState(tempMuiPropsState);
    }
    setIsAskSave(true);
  };

  const CmsFieldOnChange = (event, key): void => {
    const tempCmsPropsState = {
      ...cmsPropsState,
      [key]: event.currentTarget.value
    };
    if (!_.isEmpty(tempCmsPropsState)) {
      setCmsPropsState(tempCmsPropsState);
    }
    setIsAskSave(true);
  };

  const displayNameOnChange = (event): void => {
    setDisplayNameEn(event.target.value);
    setIsAskSave(true);
  };

  const StyleFieldOnChange = (event, key): void => {
    const tempStyleState = {
      ...styleState,
      [key]: event.currentTarget.value
    };
    if (!_.isEmpty(tempStyleState)) {
      setStyleState(tempStyleState);
    }
    setIsAskSave(true);
  };

  const returnDisplayName = (): JSX.Element => {
    console.log("onMouseIn", onMouseIn);
    // if (onMouseIn) {
    const displayObj = {
      en: displayNameEn
    };
    return (
      <TextField
        autoFocus
        value={displayNameEn}
        onChange={displayNameOnChange}
        onBlur={(): void => {
          setSelectedNodeValueToTree("displayName", displayObj);
          setOnMouseIn(false);
        }}
      />
    );
    // } else {
    //   return <span className={classes.IamText}>{en || ""}</span>;
    // }
  };

  const ReturnSelectedNodeCmsProps = (): JSX.Element => {
    console.log("[PropertyWindowSection.tsx] cmsPropsState", cmsPropsState);
    if (
      selectedTypeElement &&
      selectedTypeElement.cmsProps &&
      selectedTypeElement.cmsProps.length > 0
    ) {
      // console.warn(cmsProperty);
      return (
        <Fragment>
          <p className={classes.sectionTitle}>Cms</p>
          {selectedTypeElement.cmsProps.map((property, index) => {
            console.log(
              "[PropertyWindowSection.tsx] ReturnSelectedNodeCmsProps property:",
              property
            );
            console.log(
              "[PropertyWindowSection.tsx] ReturnSelectedNodeCmsProps cmsPropsState[property.key]:",
              cmsPropsState[property.key]
            );
            const { value = {} } = property;
            console.log(
              "[PropertyWindowSection.tsx] ReturnSelectedNodeCmsProps value]:",
              value
            );
            return (
              <div
                className={classnames(classes.styleContainer, {
                  [classes.lastElement]:
                    index === selectedTypeElement.cmsProps.length
                })}
                key={`cms_${index}`}
              >
                <div className={classes.flexRow}>
                  <p>{value.title || "title"}</p>
                  {value.title === "Src" && (
                    <ImageSearchIcon
                      onClick={(): void => setIsOpenUploadDialog(true)}
                    />
                  )}
                </div>

                {isOpenUploadDialog && (
                  <UploadFileBox
                    title={value.title || "title"}
                    initialValue={cmsPropsState[property.key] || ""}
                    propertyKey={property.key}
                    propsState={cmsPropsState}
                    fieldOnChange={CmsFieldOnChange}
                    triggerTreeUpdate={(propsState): void =>
                      setSelectedNodeValueToTree("cmsProps", propsState)
                    }
                    cancelClickFunc={(): void => setIsOpenUploadDialog(false)}
                  />
                )}
                {property.value.type === "dropdown" && (
                  <DropDownSelector
                    title={value.title}
                    dropdownList={
                      property.value.options || property.value.optons
                    }
                    initialValue={cmsPropsState[property.key]}
                    propertyKey={property.key}
                    propsState={cmsPropsState}
                    fieldOnChange={CmsFieldOnChange}
                    triggerTreeUpdate={(): void =>
                      triggerSetSelectedNodeValueToTree("cmsProps")
                    }
                  />
                )}
                {property.value.type === "boolean" && (
                  <BooleanSelector
                    title={value.title}
                    initialValue={
                      // "" means visible
                      cmsPropsState[property.key] === "" ? true : false
                    }
                    propertyKey={property.key}
                    propsState={cmsPropsState}
                    fieldOnChange={CmsFieldOnChange}
                    triggerTreeUpdate={(propsState): void =>
                      setSelectedNodeValueToTree("cmsProps", propsState)
                    }
                  />
                )}
                {property.value.type === "string" && (
                  <TextField
                    value={cmsPropsState[property.key] || ""}
                    onChange={(event): void =>
                      CmsFieldOnChange(event, property.key)
                    }
                    onBlur={(): void =>
                      setSelectedNodeValueToTree("cmsProps", cmsPropsState)
                    }
                  />
                )}
                {property.value.type === "pageSelector" && (
                  <PageSelector
                    title={value.title}
                    projectId={currentProjectId}
                    initialSelectedPage={cmsPropsState[property.key]}
                    selectPageFunc={(item: any): void => {
                      CmsFieldOnChange(item, property.key);
                      setSelectedNodeValueToTree("cmsProps", cmsPropsState);
                    }}
                  />
                )}
              </div>
            );
          })}
        </Fragment>
      );
    }
    return <span />;
  };

  const ReturnSelectedNodeMuiProps = (): JSX.Element => {
    if (
      selectedTypeElement &&
      selectedTypeElement.muiProps &&
      selectedTypeElement.muiProps.length > 0
    ) {
      // console.warn(cmsProperty);
      return (
        <Fragment>
          <p className={classes.sectionTitle}>Mui</p>
          {selectedTypeElement.muiProps.map((property, index) => {
            const { value = {} } = property;
            return (
              <div
                className={classnames(classes.styleContainer, {
                  [classes.lastElement]:
                    index === selectedTypeElement.muiProps.length
                })}
                key={`mui_${index}`}
              >
                <p>{value.title || "title"}</p>
                {property.value.type === "dropdown" && (
                  <DropDownSelector
                    // propKey={property.key}
                    title={property.value.title}
                    dropdownList={
                      property.value.options || property.value.optons
                    }
                    initialValue={muiPropsState[property.key]}
                    propertyKey={property.key}
                    propsState={muiPropsState}
                    fieldOnChange={MuiFieldOnChange}
                    triggerTreeUpdate={(): void =>
                      triggerSetSelectedNodeValueToTree("muiProps")
                    }
                  />
                )}
                {property.value.type === "string" && (
                  <TextField
                    value={muiPropsState[property.key] || ""}
                    onChange={(event): void =>
                      MuiFieldOnChange(event, property.key)
                    }
                    onBlur={(): void =>
                      setSelectedNodeValueToTree("muiProps", muiPropsState)
                    }
                  />
                )}
                {property.value.type === "object" && (
                  <Fragment>
                    <TextAreaIcon
                      onClick={(): void => setIsOpenTextArea(true)}
                    />
                    {isOpenTextArea && (
                      <TextAreaBox
                        title={property.value.title}
                        initialValue={muiPropsState[property.key]}
                        propertyKey={property.key}
                        propsState={muiPropsState}
                        fieldOnChange={MuiFieldOnChange}
                        triggerTreeUpdate={(): void =>
                          triggerSetSelectedNodeValueToTree("muiProps")
                        }
                        closeTextArae={(): void => setIsOpenTextArea(false)}
                      />
                    )}
                  </Fragment>
                )}
              </div>
            );
          })}
        </Fragment>
      );
    }
    return <span />;
  };

  const ReturnSelectedNodeStyle = (): JSX.Element => {
    // console.warn(state);
    if (
      selectedTypeElement &&
      selectedTypeElement.style &&
      selectedTypeElement.style.length > 0
    ) {
      return (
        <Fragment>
          <p className={classes.sectionTitle}>Style</p>
          {selectedTypeElement.style.map((property, index) => {
            console.log(
              "[PropertyWindowSection.tsx] ReturnSelectedNodeStyle property:",
              property
            );
            const { value = {} } = property;
            return (
              <div
                className={classnames(classes.styleContainer, {
                  [classes.lastElement]:
                    index === selectedTypeElement.style.length
                })}
                key={`cms_${index}`}
              >
                <div className={classes.flexRow}>
                  <p>{value.title || "title"}</p>
                  {value.title === "Src" && (
                    <ImageSearchIcon
                      onClick={(): void => setIsOpenUploadDialog(true)}
                    />
                  )}
                </div>

                {isOpenUploadDialog && (
                  <UploadFileBox
                    title={value.title || "title"}
                    initialValue={styleState[property.key] || ""}
                    propertyKey={property.key}
                    propsState={styleState}
                    fieldOnChange={StyleFieldOnChange}
                    triggerTreeUpdate={(propsState): void =>
                      setSelectedNodeValueToTree("style", propsState)
                    }
                    cancelClickFunc={(): void => setIsOpenUploadDialog(false)}
                  />
                )}
                {property.key === "backgroundColor" && (
                  <ColorSelector
                    title={value.title}
                    initialSelectedColor={styleState[property.key] || ""}
                    selectColorFunc={(event: any): void => {
                      StyleFieldOnChange(event, property.key);
                      setSelectedNodeValueToTree("style", styleState);
                    }}
                  />
                )}

                {property.key !== "backgroundColor" && (
                  <TextField
                    value={styleState[property.key] || ""}
                    onChange={(event): void =>
                      StyleFieldOnChange(event, property.key)
                    }
                    onBlur={(): void =>
                      setSelectedNodeValueToTree("style", styleState)
                    }
                  />
                )}
              </div>
            );
          })}
        </Fragment>
      );
    }
    return <span />;
  };

  return (
    <div className={classes.proWindow}>
      {_.isEmpty(selectedTreeNode) ? (
        <div />
      ) : (
        <Fragment>
          <div
            className={classes.selectedTreeDisplayName}
            onClick={(): void => {
              setOnMouseIn(true);
            }}
          >
            {returnDisplayName()}
          </div>
          {ReturnSelectedNodeStyle()}
          {ReturnSelectedNodeCmsProps()}
          {ReturnSelectedNodeMuiProps()}
        </Fragment>
      )}
    </div>
  );
};

export default PropertyWindowSection;
