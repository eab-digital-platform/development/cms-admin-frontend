import React, { FunctionComponent, useContext } from "react";
import { RouteContext } from "./contextComponent";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { routeType } from "./type/enum";
import BaristaBarIcon from "./assets/BaristaBarIcon.png";
import { findCodeById, findTitleById } from "./common/commonFunction";
import { savePage } from "./api";

// import React, { FunctionComponent, useContext } from "react";
// import { RouteContext } from "./contextComponent";
// import classnames from "classnames";
// import { routeType } from "./type/enum";

interface HeaderSectionProps {
  headerTitle: string;
}

const HeaderSection: FunctionComponent<HeaderSectionProps> = (
  props: HeaderSectionProps
) => {
  const { AddHome, Index, AddProject } = routeType;
  const useStyles = makeStyles({
    headerContainer: {
      padding: "var(--Apadding-L) var(--Apadding-S)",
      display: "grid",
      gridTemplateColumns: "220px 1fr 220px",
      alignItems: "center",
      justifyContent: "space-between",
      background: "var(--Aheader-background)",
      position: "fixed",
      width: "100%",
      zIndex: 9999
    },
    titleWrapper: {
      display: "flex",
      color: "var(--Awhite-font-color)",
      alignItems: "center",
      height: "36px",
      fontWeight: "bold",
      fontSize: "var(--Afont-M)"
    },
    rightIconWrapper: {
      width: "222px",
      textAlign: "center"
    },
    coffeeIcon: {
      height: "16px",
      padding: "var(--Apadding-XS)",
      boxSizing: "unset"
    },
    goBackContainer: {
      cursor: "pointer",
      width: "120px",
      display: "flex",
      overflow: "hidden",
      alignItems: "center",
      textOverflow: "ellipsis",
      whiteSpace: "nowrap"
    },
    addIcon: {
      marginRight: "var(--Apadding-XXXM)"
    },
    infoIcon: {
      width: "18px",
      height: "18px",
      margin: "0 var(--Apadding-XXS) 0 var(--Apadding-XXL)",
      color: "var(--Awhite-font-color)"
    },
    searchBar: {
      width: "1046px"
    },
    textInputStyle: {
      padding: "var(--Apadding-XXXS) var(--Apadding-S)"
    },
    backRoot: {
      color: "var(--Awhite-font-color)"
    },
    inline: {
      display: "flex"
    },
    boldClickable: {
      fontWeight: "bold",
      cursor: "pointer",
      marginLeft: "var(--Apadding-XXXXS)"
    }
  });
  // const useMuiStyle = makeStyles(
  //   {
  //     input: {
  //       "&.MuiInputBase-input": {
  //         padding: "8.5px 14px"
  //       }
  //     }
  //   },
  //   { name: "MuiOutlinedInput" }
  // );
  const { headerTitle } = props;
  const classes = useStyles(props);
  // const muiClasses = useMuiStyle(props);
  const { state, func } = useContext(RouteContext);
  const {
    treeDataState,
    routeState,
    currentPageId,
    currentProjectId,
    pageList,
    projectList
  } = state;
  const { setRouteState, openErrorDialogWithMsg, setIsAskSave } = func;
  const returnRootSection = (): JSX.Element | null => {
    let pageCode = "",
      projectCode = "";
    if (projectList && projectList.length > 0 && currentProjectId) {
      projectCode = findTitleById(projectList, currentProjectId);
    }
    if (pageList && pageList.length > 0 && currentPageId) {
      pageCode = findCodeById(pageList, currentPageId);
    }
    if (pageCode) {
      return (
        <div className={classes.inline}>
          {projectCode && `${projectCode} >`}
          <p
            onClick={(): void => setRouteState(Index)}
            className={classes.boldClickable}
          >
            {pageCode}
          </p>
        </div>
      );
    } else {
      return null;
    }
  };

  const onSave = (): void => {
    // treeDataState;
    console.warn("I am saving");
    setIsAskSave(false);
    savePage(currentPageId, treeDataState[0])
      .then(res => {
        console.warn(res);
      })
      .catch(err => {
        openErrorDialogWithMsg(err.response.data.message);
      });
  };

  return (
    <div className={classes.headerContainer}>
      <div className={classes.titleWrapper}>
        <div
          className={classes.goBackContainer}
          onClick={(): void => setRouteState(AddProject)}
        >
          <img
            src={BaristaBarIcon}
            alt={headerTitle}
            className={classes.coffeeIcon}
          />
          {headerTitle}
        </div>
        <div></div>
      </div>
      <div className={classes.backRoot}>
        {routeState === AddHome && returnRootSection()}
      </div>

      {/* <div className="searchBar">
        <TextField
          className={classnames(classes.searchBar, muiClasses.input)}
          fullWidth
          margin="none"
          id="outlined-basic"
          variant="outlined"
          onChange={(e): void => {
            func.setRouteState(e.target.value);
          }}
          placeholder={state.routeState}
        />
      </div> */}
      {routeState === AddHome && (
        <div className={classes.rightIconWrapper}>
          <Button
            variant="contained"
            color="primary"
            onClick={onSave}
            className={classes.addIcon}
          >
            Save
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => func.setRouteState(AddHome)}
            className={"Preview"}
          >
            Publish
          </Button>
        </div>
      )}
    </div>
  );
};

export default HeaderSection;
