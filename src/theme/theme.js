import { createMuiTheme } from "@material-ui/core";

// Here is the Theme of the Material UI.

// This function take the name of the variable in CSS.
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
function getDefaultTheme(property) {
  return getComputedStyle(document.body)
    .getPropertyValue(property)
    .trim();
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default function customTheme() {
  return createMuiTheme({
    typography: {
      fontFamily: getDefaultTheme("--font-family"),
      useNextVariants: true
    },
    palette: {
      primary: {
        main: getDefaultTheme("--btn-primary-background")
      },
      secondary: {
        main: getDefaultTheme("--btn-secondary-background")
      },
      text: {
        white: getDefaultTheme("--font-white"),
        black: getDefaultTheme("--font-black"),
        lightRed: getDefaultTheme("--font-lightRed"),
        grey: getDefaultTheme("--font-grey"),
        darkGrey: getDefaultTheme("--font-darkGrey")
      }
    },
    overrides: {
      MuiButton: {
        contained: {
          "box-shadow": "none",
          background: getDefaultTheme("--btn-default-background")
        }
      },
      MuiExpansionPanelSummary: {
        root: {
          "min-height": 0,
          "&$expanded": {
            "min-height": 0
          }
        }
      },
      MuiFab: {
        root: {
          "box-shadow": "none"
        }
      },
      MuiPaper: {
        elevation2: {
          "box-shadow": "none",
          border: `${getDefaultTheme("--borderWidthS")} solid ${getDefaultTheme(
            "--paper-border"
          )}`
        }
      },
      MuiTooltip: {
        tooltip: {
          backgroundColor: "transparent",
          maxWidth: "none",
          lineHeight: 0
        },
        popper: {
          opacity: 1
        }
      },
      MuiInput: {
        underline: {
          /** strange MUI Behaviour */
          "&:hover:not($disabled):not($focused):not($error):before": {
            borderBottom: `${getDefaultTheme(
              "--borderWidthM"
            )} solid ${getDefaultTheme("--color-grey")}`
          },
          "&:after": {
            borderBottom: `${getDefaultTheme(
              "--borderWidthM"
            )} solid ${getDefaultTheme("--color-grey")}`
          },
          "&.Mui-disabled:before": {
            borderBottomStyle: "solid"
          }
        }
      },
      MuiInputBase: {
        input: {
          "&.Mui-disabled": {
            cursor: "not-allowed"
          }
        }
      },
      MuiSnackbarContent: {
        root: {
          width: "calc(100vw *(900/1440))",
          justifyContent: "space-between"
        },
        message: {
          display: "inherit",
          alignItems: "center",
          flex: "1",
          justifyContent: "center",
          color: getDefaultTheme("--snackbar-text")
        }
      },
      MuiFormControlLabel: {
        root: {
          "&.Mui-disabled": {
            color: getDefaultTheme("--font-black"),
            cursor: "not-allowed"
          }
        }
      }
    }
  });
}
