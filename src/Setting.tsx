import React, {
  FunctionComponent,
  useState,
  useContext,
  useEffect
} from "react";
import { RouteContext } from "./contextComponent";
import {
  Button,
  TextField,
  List,
  ListItem,
  ListItemText,
  FormControl,
  InputLabel,
  Select,
  Divider
} from "@material-ui/core";
// import List from '@material-ui/core/List';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from "@material-ui/core/styles";
import { PresetDetail, ProjectSettingInterface } from "./type/interfaceCommon";
import {
  findDescriptionById,
  findTitleById,
  findSettingsByPjId
} from "./common/commonFunction";
import { updateProject, getListOfProject } from "./api";
import { parseProjectList } from "./parser";
import { routeType } from "./type/enum";

import EnvironmentVariableSetting from "./components/EnvironmentVariableSetting/EnvironmentVariableSetting";
import { getProjectSettingById, getListOfPage } from "./api";
import Styles from "./Setting.module.css";

export interface SettingUIProps {
  title?: string;
  content?: string | JSX.Element;
  open?: boolean;
  onConfirm?: () => any;
  onClose?: () => any;
}

const SettingUI: FunctionComponent<SettingUIProps> = (
  props: SettingUIProps
) => {
  const { state, func } = useContext(RouteContext);
  const { projectList, currentProjectId } = state;
  const { openErrorDialogWithMsg, setProjectList, setRouteState } = func;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [projectDetail, setProjectDetail] = useState<PresetDetail>({
    code: "",
    description: ""
  });
  const [seletedCssTheme, setSelectedCssTheme] = useState<string>(""); //Should be call api for init

  const inputLabel = React.useRef<HTMLLabelElement>(null);

  // const tempPgId = "5e58b144ccba8400328e2534";
  const tempPgId = currentProjectId;
  const [projectName, setProjectName] = useState<string>("");
  const [projectDescription, setProjectDescription] = useState<string>("");
  const [startPage, setStartPage] = useState<string>("");
  const [themeStyle, setThemeStyle] = useState<string>("");
  const [envVarPair, setEnvVarPair] = useState<Array<Record<string, any>>>([
    { name: "", value: "" }
  ]);
  const [projectPageList, setProjectPageList] = useState([
    { _id: "", code: "" }
  ]);

  useEffect(() => {
    getProjectSettingById(tempPgId).then(res => {
      setProjectName(res.title.en);
      setProjectDescription(res.description.en);
      setStartPage(res.settings.firstPageId);
      setThemeStyle(res.settings.theme);
      setEnvVarPair([
        ...res.settings.environmentVariableSetting,
        { name: "", value: "" }
      ]);
    });
  }, []);

  useEffect(() => {
    getListOfPage(tempPgId).then(res => {
      setProjectPageList(res);
    });
  }, []);

  useEffect(() => {
    if (projectList && projectList.length > 0 && currentProjectId) {
      const pjCode = findTitleById(projectList, currentProjectId);
      const pjDescription = findDescriptionById(projectList, currentProjectId);
      const pjSettings: ProjectSettingInterface = findSettingsByPjId(
        projectList,
        currentProjectId
      );
      setSelectedCssTheme(pjSettings.theme);
      setProjectDetail({
        code: pjCode,
        description: pjDescription
      });
    }
  }, []);

  // const { code, description } = projectDetail;
  // const setProjectCode = (event): void => {
  //   setProjectDetail({
  //     ...projectDetail,
  //     code: event.target.value
  //   });
  // };

  // const setProjectDescriptionHandler = (event): void => {
  //   setProjectDetail({
  //     ...projectDetail,
  //     description: event.target.value
  //   });
  // };

  const onConfirmHandler = (): void => {
    const data = {
      title: {
        en: projectName
      },
      description: {
        en: projectDescription
      },
      settings: {
        theme: themeStyle,
        environmentVariableSetting: envVarPair,
        firstPageId: startPage
      }
    };

    updateProject(tempPgId, data)
      .then(() => {
        return getListOfProject();
      })
      .then(data => {
        const projectList = parseProjectList(data);
        setProjectList(projectList);
        // setRouteState(routeType.AddProject);
        setRouteState(routeType.Index);
      })
      .catch(err => {
        console.log(err);
        openErrorDialogWithMsg(err.response.data.message);
      });
  };

  // const onConfirm = (): void => {
  //   const data = {
  //     title: {
  //       en: projectDetail.code
  //     },
  //     description: {
  //       en: projectDetail.description
  //     },
  //     settings: {
  //       theme: seletedCssTheme,
  //       sourceHost: []
  //     }
  //   };
  //   updateProject(currentProjectId, data)
  //     .then(() => {
  //       return getListOfProject();
  //     })
  //     .then(data => {
  //       const projectList = parseProjectList(data);
  //       setProjectList(projectList);
  //       setRouteState(routeType.AddProject);
  //     })
  //     .catch(err => {
  //       openErrorDialogWithMsg(err.response.data.message);
  //     });
  // };

  const listItemOnClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    name: string
  ): void => {
    setSelectedCssTheme(name);
  };

  const changeDropDownSelection = (
    event: React.ChangeEvent<{ value: unknown }>
  ): void => {
    setStartPage(event.target.value as string);
  };

  const changeHandler = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ): void => {
    const updatedList = [...envVarPair];
    updatedList[index][e.target.name] = e.target.value;
    setEnvVarPair(updatedList as Array<Record<string, any>>);
  };

  const addEnvironmentSettingPair = (): void => {
    const updatedList = [...envVarPair];
    updatedList.push({ name: "", value: "" });
    setEnvVarPair(updatedList as Array<Record<string, any>>);
  };

  const deleteHandler = (index: number): void => {
    const updatedList = [...envVarPair];
    updatedList.splice(index, 1);
    setEnvVarPair(updatedList as Array<Record<string, any>>);
  };

  const useStyles = makeStyles({
    settingContainer: {
      height: "80%",
      overflowX: "auto",
      overflowY: "auto",
      padding:
        "var(--Apadding-XXXL) var(--Apadding-SEXL) var(--Apadding-XXXL) var(--Apadding-XXXL)",
      "& p": {
        fontWeight: "bold",
        color: "var(--Ablack)",
        marginBottom: "var(--Apadding-M)"
      },
      "& .MuiOutlinedInput-root": {
        height: "36px"
      }
    },
    upperLevel: {
      width: "258px",
      "& .MuiFormControl-root.MuiTextField-root": {
        marginBottom: "var(--Apadding-46)"
      }
    },
    contentLevel: {
      display: "flex",
      paddingTop: "var(--Apadding-46)",
      height: "324px"
    },
    left: {
      paddingRight: "var(--Apadding-46)",
      width: "258px",
      height: "100%"
    },
    listMain: {
      border: "var(--Aprimary-color-light2) var(--Aborder-S) solid"
    },
    buttonWrapper: {
      textAlign: "right"
    }
  });
  const classes = useStyles(props);

  return (
    <div className={classes.settingContainer}>
      <h2>Project Detail Setting</h2>

      <div className={Styles.ProjDetailSettingRow}>
        <h3 className={Styles.ProjDetailSettinLeft}>Project Name</h3>
        <h3 className={Styles.ProjDetailSettinRight}>Project Description</h3>
      </div>

      <div className={Styles.ProjDetailSettingRow}>
        <div className={Styles.ProjDetailSettinLeft}>
          <TextField
            autoFocus
            fullWidth
            margin="none"
            id="outlined-basic"
            variant="outlined"
            value={projectName}
            name="projectName"
            // className={classText.root}
            onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
              setProjectName(e.target.value)
            }
          />
        </div>

        <div className={Styles.ProjDetailSettinRight}>
          <TextField
            fullWidth
            margin="none"
            id="outlined-basic"
            variant="outlined"
            value={projectDescription}
            // className={classText.root}
            onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
              setProjectDescription(e.target.value)
            }
          />
        </div>
      </div>

      <div className={Styles.ProjDetailSettingRow}>
        <h3 className={Styles.ProjDetailSettinLeft}>Startup Page</h3>
        <h3 className={Styles.ProjDetailSettinRight}></h3>
      </div>

      <div className={Styles.ProjDetailSettingRow}>
        <div className={Styles.ProjDetailSettinLeft}>
          <FormControl fullWidth variant="outlined">
            <InputLabel ref={inputLabel} htmlFor="filled-age-native-simple">
              {/* List */}
            </InputLabel>
            <Select
              value={startPage}
              onChange={changeDropDownSelection}
              inputProps={{
                name: "startPage",
                id: "filled-age-native-simple"
              }}
            >
              <option value="" />
              {projectPageList.map((page, index) => {
                return (
                  <option
                    className={Styles.ListOption}
                    value={page._id}
                    key={index}
                  >
                    {page.code}
                  </option>
                );
              })}
            </Select>
          </FormControl>
        </div>
        <div className={Styles.ProjDetailSettinRight}></div>
      </div>

      <Divider />

      <div className={classes.contentLevel}>
        <div className={classes.left}>
          <h2>Theme Style</h2>
          <List
            component="nav"
            aria-label="main mailbox folders"
            className={classes.listMain}
          >
            <ListItem
              button
              selected={seletedCssTheme === "Basic"}
              onClick={(
                event: React.MouseEvent<HTMLDivElement, MouseEvent>
              ): void => listItemOnClick(event, "Basic")}
            >
              <ListItemText primary="Basic" />
            </ListItem>
            <ListItem
              button
              selected={seletedCssTheme === "Theme2"}
              onClick={(
                event: React.MouseEvent<HTMLDivElement, MouseEvent>
              ): void => listItemOnClick(event, "Theme2")}
            >
              <ListItemText primary="Theme2" />
            </ListItem>
          </List>
        </div>

        <div className={"right"}>
          <p>Preview</p>
          <div>{seletedCssTheme}</div>
        </div>
      </div>

      <Divider />

      <EnvironmentVariableSetting
        envValPairList={envVarPair}
        onChanged={changeHandler}
        addNew={addEnvironmentSettingPair}
        deletePair={deleteHandler}
      />

      <hr></hr>
      <div className={classes.buttonWrapper}>
        {/* <Button onClick={onConfirm} variant="contained" color="primary"> */}
        <Button onClick={onConfirmHandler} variant="contained" color="primary">
          Confirm
        </Button>
      </div>
    </div>
  );
};

export default SettingUI;
