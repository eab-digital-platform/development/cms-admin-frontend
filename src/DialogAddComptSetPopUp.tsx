import React, { FunctionComponent, useContext } from "react";
import { TextField, Button } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import { RouteContext } from "./contextComponent";

import { DialogBasicProps } from "./type/interfaceCommon";
import { addPjComponentSet, getComponentSetList } from "./api";
interface DialogAddComptSetProps extends DialogBasicProps {
  comptSetName: string;
  setComptSetName: Function;
  comptSetDesc: string;
  setSomptSetDesc: Function;
  projectId: string;
}

const DialogAddComptSetDialog: FunctionComponent<DialogAddComptSetProps> = (
  props: DialogAddComptSetProps
) => {
  const { func } = useContext(RouteContext);
  const { setPjComptSetList } = func;
  const {
    dialogOpen,
    closeDialog,
    comptSetName,
    setComptSetName,
    comptSetDesc,
    setSomptSetDesc,
    projectId
  } = props;

  const clickedHandler = (): void => {
    const data = {
      // TODO: add data
      title: comptSetName,
      description: comptSetDesc,
      style: {},
      cmsProps: {},
      muiProps: {},
      items: [{}],
      system: {
        createBy: "admin",
        updateBy: "admin"
      }
    };

    addPjComponentSet(projectId, data)
      .then(() => {
        getComponentSetList(projectId).then(data => {
          setPjComptSetList(data);
          closeDialog();
        });
        setComptSetName("");
        setSomptSetDesc("");
      })
      .catch(err => {
        console.log(err);
      });
  };

  return (
    <Dialog open={dialogOpen} onClose={closeDialog} fullWidth maxWidth={"sm"}>
      <DialogTitle>
        <DialogTitle>Add Component Set</DialogTitle>
      </DialogTitle>
      <DialogContent dividers>
        <TextField
          autoFocus
          fullWidth
          // className={classes.pageNmae}
          variant="outlined"
          value={comptSetName}
          onChange={(e): void => setComptSetName(e.target.value)}
          placeholder={"Component Set Name"}
        />
        <TextField
          fullWidth
          variant="outlined"
          value={comptSetDesc}
          onChange={(e): void => setSomptSetDesc(e.target.value)}
          placeholder={"Component Set Description"}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog} variant="contained" color="primary">
          Cancel
        </Button>
        <Button
          disabled={comptSetName === "" || comptSetDesc === "" ? true : false}
          onClick={clickedHandler}
          variant="contained"
          color="primary"
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogAddComptSetDialog;
