import { Selector } from "testcafe";

fixture`GettingStarted`.page`http://cms-admin-frontend.dp.k8s-dev/`;

test("Left Menu should have mandatory UI components", async t => {
  const isLeftMenuProjectExist = Selector("#leftMenu_Project").exists;
  const leftMenuProjectText = Selector("#leftMenu_Project").textContent;
  const isLeftMenuPageExist = Selector("#leftMenu_Page").exists;
  const leftMenuPageText = Selector("#leftMenu_Page").textContent;
  const isLeftMenuComponentSetExist = Selector("#leftMenu_ComponentSet").exists;
  const leftMenuComponentSetText = Selector("#leftMenu_ComponentSet")
    .textContent;
  await t
    .expect(isLeftMenuProjectExist)
    .ok()
    .expect(leftMenuProjectText)
    .eql("Project")
    .expect(isLeftMenuPageExist)
    .ok()
    .expect(leftMenuPageText)
    .eql("Page")
    .expect(isLeftMenuComponentSetExist)
    .ok()
    .expect(leftMenuComponentSetText)
    .eql("Component Set");
});

test("Search input box should have mandatory UI components", async t => {
  const isSearchInputBoxExist = Selector("#searchTextField").exists;
  const placeholder = Selector("#searchTextField").getAttribute("placeholder");
  await t
    .expect(isSearchInputBoxExist)
    .ok()
    .expect(placeholder)
    .eql("Search Projects...");
});

test("Search input box should let user enter text and can be display on UI", async t => {
  const searchTextField = Selector("#searchTextField");
  const randomSearchKey = "fdsjkfjdslfkdsjfldskj";
  await t.typeText(searchTextField, randomSearchKey);
  await t.takeScreenshot();
  const userInput = await Selector("#searchTextField").getAttribute("value");
  await t.expect(userInput).eql(randomSearchKey);
});

test("Redirect to pages page and redirect back", async t => {
  await t.click("#leftMenu_Page").takeScreenshot();
  await t.click("#leftMenu_Project").takeScreenshot();
});

test("Redirect to component set page and redirect back", async t => {
  await t.click("#leftMenu_ComponentSet").takeScreenshot();
  await t.click("#leftMenu_Project").takeScreenshot();
});

test("Redirect to pages page, then redirect to component set page, finally redirect back", async t => {
  await t.click("#leftMenu_Page").takeScreenshot();
  await t.click("#leftMenu_ComponentSet").takeScreenshot();
  await t.click("#leftMenu_Project").takeScreenshot();
});

test("Redirect to component set page, then redirect to pages page, finally redirect back", async t => {
  await t.click("#leftMenu_ComponentSet").takeScreenshot();
  await t.click("#leftMenu_Page").takeScreenshot();
  await t.click("#leftMenu_Project").takeScreenshot();
});
